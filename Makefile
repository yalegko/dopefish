init:
	@echo "Run the following line in the shell or add to profile init script (.bashrc or same)"
	@echo "export PYTHONPATH=${PYTHONPATH}:`pwd`"

test:
	nosetests -v tests/*