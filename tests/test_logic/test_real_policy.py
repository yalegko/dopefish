import z3
from nose.tools import assert_equals, assert_true

from dopefish import constraints, ConstraintsFactory
from dopefish.dopetypes import DopeString
from dopefish.dopetypes.dopeset import DopeStringSet

interfaces = """
interface Entity {
  abstract id: str;
}

[angine=entity]
interface UrlEntity <: Entity {
  path: str;
  level: int;
  tags: [str];
}

[angine=subject]
interface Subject <: Entity {
  name: str;
  roles: [str];
  level: int;
  tags: [str];
  abstract ip: str;
}
"""

policy = """
namespace example {
    export policySet mainPolicy {
        apply denyUnlessPermit
        policy postMotd {
            target clause action == "POST" and entity.path == "/motd"
            apply denyUnlessPermit
            rule r11 {
                permit
                target clause "admin" in subject.roles 
            }
        }
        policy getAdmin {
            target clause action == "GET" and entity.path == "/admin"
            apply denyUnlessPermit
            rule r12 {
                permit
                target clause "admin" in subject.roles
            }
        }
        policy getStats {
            target clause action == "GET" and entity.path == "/stats"
            apply denyUnlessPermit
            rule r13 {
                permit
                target clause subject.ip == "127.0.0.1"
            }
        }
    }
}
"""

def test_solving_simple():
    policy_constraints = ConstraintsFactory.build_tree(
        alfa_policy=policy,
        idl_interfaces=interfaces
    )
    # Extract the first policy (set) from the namespace
    constraint = policy_constraints.policies[0]
    assert isinstance(constraint, constraints.PolicySet)

    solver = z3.Solver()
    solver.set("timeout", 20000)

    # Base policy constraints
    solver.add(constraint.permit)
    solver.add(z3.Not(constraint.deny))
    solver.add(z3.Not(constraint.not_applicable))
    solver.add(z3.Not(constraint.indeterminate))
    solver.add(policy_constraints.additional_constraints)

    # Additional constraints
    # 1. subject.roles should be subset of ["guest", "user", "admin"]
    roles = policy_constraints.attributes["subject.roles"]
    allowed_roles = DopeStringSet.from_values(["guest", "user", "admin"])
    solver.add(roles.issubseteq(allowed_roles).get_constraint())
    solver.add(allowed_roles.additional_constraints)
    # 2. Subject can't have `admin` rights
    solver.add(roles.contains(DopeString("admin")).get_constraint(False))

    # Solver should say us that we can get PERMIT using localhost IP with any role.
    assert_equals(solver.check(), z3.sat,
                  "Failed to find solution via localhost subject.ip")
    assert_true(solver.model().evaluate(
                  z3.String("subject.ip") == z3.StringVal("127.0.0.1")),
                  "Found unexpected solution")

    # And this is the only possible way.
    solver.add(z3.String("subject.ip") != z3.StringVal("127.0.0.1"))
    assert_equals(solver.check(), z3.unsat,
                  "Found unexpected solution without localhost subject.ip")


