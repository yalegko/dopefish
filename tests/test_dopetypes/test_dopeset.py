import z3
from nose.tools import assert_true, assert_greater, assert_equals, assert_not_equals

from dopefish.dopetypes import DopeStringSet, DopeIntSet, DopeInt


def test_dopestringset_specific():
    assert_true(DopeStringSet.element_sort().is_string(),
                "DopeStringSet element sort is not a string")

    s = DopeStringSet.element_var("s")
    assert_true(z3.is_const(s),
                "Created something that isn't variable")
    assert_true(z3.is_string(s),
                "Created variable isn't string")

    s = DopeStringSet.element_value("dopekek")
    assert_true(z3.is_string_value(s),
                "Failed to create string constant")

    s = DopeStringSet.create_variable("s")
    assert_true(z3.is_array(z3.simplify(s.get_val())),
                "Failed to create array variable")


def test_dopeintset_specific():
    assert_true(DopeIntSet.element_sort().is_int(),
                "DopeIntSet element sort is not an integer")

    x = DopeIntSet.element_var("x")
    assert_true(z3.is_const(x),
                "Created something that isn't variable")
    assert_true(z3.is_int(x),
                "Created variable isn't an integer")

    x = DopeIntSet.element_value(42)
    assert_true(z3.is_int_value(x),
                "Failed to create integer constant")

    x = DopeIntSet.create_variable("x")
    assert_true(z3.is_array(z3.simplify(x.get_val())),
                "Failed to create array variable")


def test_dopeintset_from_values():
    values = [7, 77, 777]
    dopeset = DopeIntSet.from_values(values)
    assert_true(z3.is_array(z3.simplify(dopeset.get_val())),
                "Failed to create array from values")
    assert_greater(len(dopeset.additional_constraints), 0,
                   "Failed to create additional constraints")

    solver = z3.Solver()
    solver.add(dopeset.additional_constraints)
    solver.push()

    x = z3.Int("x")
    for i, v in enumerate(values):
        solver.add(z3.simplify(z3.Exists([x], dopeset.get_val()[x] == v)))
        assert_equals(solver.check(), z3.sat,
                      "Failed to find #{} value in modelling array".format(i))
        solver.pop()
        solver.push()

    solver.add(z3.simplify(z3.Exists([x], dopeset.get_val()[x] == 42)))
    assert_equals(solver.check(), z3.unsat,
                  "Found non-existent value in the array")


def test_dopestringset_from_values():
    values = ["lol", "kek", "keckulechek"]
    dopeset = DopeStringSet.from_values(values)
    assert_true(z3.is_array(z3.simplify(dopeset.get_val())),
                "Failed to create array from values")
    assert_greater(len(dopeset.additional_constraints), 0,
                   "Failed to create additional constraints")

    solver = z3.Solver()
    solver.add(dopeset.additional_constraints)
    solver.push()

    x = z3.Int("x")
    for i, v in enumerate(values):
        solver.add(z3.simplify(z3.Exists([x], dopeset.get_val()[x] == z3.StringVal(v))))
        assert_equals(solver.check(), z3.sat,
                      "Failed to find #{} value in modelling array".format(i))
        solver.pop()
        solver.push()

    solver.add(z3.simplify(z3.Exists([x], dopeset.get_val()[x] == z3.StringVal("cheburek"))))
    assert_equals(solver.check(), z3.unsat,
                  "Found non-existent value in the array")

def test_dopeset_contains():
    values = [7, 77, 777]
    dopeset = DopeIntSet.from_values(values)

    solver = z3.Solver()
    solver.add(dopeset.additional_constraints)
    solver.push()

    for i, v in enumerate(values):
        solver.add(dopeset.contains(DopeInt(v)).is_true())
        assert_equals(solver.check(), z3.sat,
                      "Failed to find #{} value in modelling array".format(i))
        solver.pop()
        solver.push()

    solver.add(dopeset.contains(DopeInt(42)).is_true())
    assert_equals(solver.check(), z3.unsat,
                  "Found non-existent value in the array")


# Following tests can be slow and not deterministic, so just check that
# it is not unsat

def test_dopeset_issubseteq():
    values = [7, 77, 777]
    dopeset = DopeIntSet.from_values(values)

    solver = z3.Solver()
    solver.set("timeout", 3000)
    solver.add(dopeset.additional_constraints)
    solver.push()

    subset = DopeIntSet.from_values([7, 77])
    solver.add(subset.additional_constraints)
    solver.add(subset.issubseteq(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to trait {} as a subset of {}".format(
                      subset.inner_z3var, dopeset.inner_z3var
                      ))
    solver.pop()
    solver.push()

    subset = DopeIntSet.from_values([777])
    solver.add(subset.additional_constraints)
    solver.add(subset.issubseteq(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to trait {} as a subset of {}".format(
                      subset.inner_z3var, dopeset.inner_z3var
                  ))
    solver.pop()
    solver.push()

    subset = DopeIntSet.from_values([7, 42])
    solver.add(subset.additional_constraints)
    solver.add(subset.issubseteq(dopeset).is_true())
    assert_equals(solver.check(), z3.unsat,
                  "Trait {} as a subset of {}".format(
                      subset.inner_z3var, dopeset.inner_z3var
                  ))
    solver.pop()
    solver.push()

    # That guy is really slow, so lower the number of values
    values = [7, 77]
    dopeset = DopeIntSet.from_values(values)
    subset = DopeIntSet.from_values(values)
    solver.add(dopeset.additional_constraints)
    solver.add(subset.additional_constraints)
    solver.add(subset.issubseteq(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to trait {} as a subset of {}".format(
                      subset.inner_z3var, dopeset.inner_z3var
                  ))


def test_dopeset_issubset():
    values = [7, 77, 777]
    dopeset = DopeIntSet.from_values(values)

    solver = z3.Solver()
    solver.set("timeout", 3000)
    solver.add(dopeset.additional_constraints)
    solver.push()

    subset = DopeIntSet.from_values([7, 77])
    solver.add(subset.additional_constraints)
    solver.add(subset.issubseteq(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to trait {} as a subset of {}".format(
                      subset.inner_z3var, dopeset.inner_z3var
                  ))
    solver.pop()
    solver.push()

    subset = DopeIntSet.from_values([7, 42])
    solver.add(subset.additional_constraints)
    solver.add(subset.issubset(dopeset).is_true())
    assert_equals(solver.check(), z3.unsat,
                  "Trait {} as a subset of {}".format(
                      subset.inner_z3var, dopeset.inner_z3var
                  ))
    solver.pop()
    solver.push()

    # That guy is really slow, so lower the number of values
    values = [7, 77]
    dopeset = DopeIntSet.from_values(values)
    subset = DopeIntSet.from_values(values)
    solver.add(dopeset.additional_constraints)
    solver.add(subset.additional_constraints)
    solver.add(subset.issubset(dopeset).is_true())
    assert_equals(solver.check(), z3.unsat, # !!!
                  "Failed to trait {} as a subset of {}".format(
                      subset.inner_z3var, dopeset.inner_z3var
                  ))


def test_dopeset_anyof():
    values = [7, 77, 777]
    dopeset = DopeIntSet.from_values(values)

    solver = z3.Solver()
    solver.set("timeout", 3000)
    solver.add(dopeset.additional_constraints)
    solver.push()

    subset = DopeIntSet.from_values([7, 33, 42])
    solver.add(subset.additional_constraints)
    solver.add(subset.intersects(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to find {} from a set {}".format(7, subset.inner_z3var))
    solver.pop()
    solver.push()

    subset = DopeIntSet.from_values([33, 777, 42])
    solver.add(subset.additional_constraints)
    solver.add(subset.intersects(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to find {} from a set {}".format(777, subset.inner_z3var))
    solver.pop()
    solver.push()

    subset = DopeIntSet.from_values([33, 42, 77])
    solver.add(subset.additional_constraints)
    solver.add(subset.intersects(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to find {} from a set {}".format(77, subset.inner_z3var))
    solver.pop()
    solver.push()

    subset = DopeIntSet.from_values([33, 42, 228, 77])
    solver.add(subset.additional_constraints)
    solver.add(subset.intersects(dopeset).is_true())
    assert_not_equals(solver.check(), z3.unsat,
                  "Failed to find {} from a set {}".format(77, subset.inner_z3var))
    solver.pop()
    solver.push()

    subset = DopeIntSet.from_values([33, 42, 228])
    solver.add(subset.additional_constraints)
    solver.add(subset.intersects(dopeset).is_true())
    assert_equals(solver.check(), z3.unsat,
                  "Found intersection of the distinct sets")
