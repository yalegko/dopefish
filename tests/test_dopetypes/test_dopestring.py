import z3
from nose.tools import assert_equal, assert_not_equal, assert_true, assert_raises

from dopefish.dopetypes import DopeString


def test_dopestring_variable():
    x = DopeString.create_z3_variable("x")
    assert_true(z3.is_const(x),
                "Created something that isn't variable")
    assert_true(z3.is_string(x),
                "Created variable isn't string")

    x = DopeString.create_variable("x")
    assert_true(z3.is_const(z3.simplify(x.get_val())),
                "Created something that isn't variable")
    assert_true(z3.is_string(z3.simplify(x.get_val())),
                "Created variable isn't string")

    # /shrug just for coverage
    assert_equal(DopeString.default_value(), "",
                 "Wrong DopeString default")


def test_dopestring_building():
    dope = DopeString("const")
    assert_true(z3.is_string_value(z3.simplify(dope.get_val())),
                "Failed to create string constant")

    dope = DopeString(z3.String('x'))
    assert_true(z3.is_string(z3.simplify(dope.get_val())),
                "Failed to create string variable")

    def create_not_string(shit):
        DopeString(0xbabe)
    assert_raises(ValueError, create_not_string,
                "Failed to create string variable")


def test_dopestring_operators_eq():
    x = z3.String('x')

    solver = z3.Solver()
    solver.add((DopeString(x) == DopeString("kek")).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x].as_string().strip('"'), "kek",
                 "Failed to model `==` operator")

    solver = z3.Solver()
    solver.add((DopeString(x) != DopeString("kek")).is_true())
    assert solver.check() == z3.sat
    assert_not_equal(solver.model()[x].as_string().strip('"'), "kek",
                 "Failed to model `!=` operator")
