import z3
from nose.tools import assert_equal, assert_not_equal, assert_true

from dopefish.dopetypes import DopeInt


def test_dopeint_variable():
    x = DopeInt.create_z3_variable("x")
    assert_true(z3.is_const(x),
                "Created something that isn't variable")
    assert_true(z3.is_int(x),
                "Created variable isn't integer")


def test_dopeint_operators_eq():
    x = z3.Int('x')

    solver = z3.Solver()
    solver.add((DopeInt(x) == DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x].as_long(), 42,
                 "Failed to model `==` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) != DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_not_equal(solver.model()[x].as_long(), 42,
                 "Failed to model `!=` operator")


def test_dopeint_operators_arithmetic():
    x = z3.Int('x')

    solver = z3.Solver()
    solver.add((DopeInt(x) == DopeInt(40) + DopeInt(2)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x].as_long(), 42,
                 "Failed to model `+` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) == DopeInt(44) - DopeInt(2)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x].as_long(), 42,
                 "Failed to model `-` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) == DopeInt(21) * DopeInt(2)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x].as_long(), 42,
                 "Failed to model `*` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) == DopeInt(84) / DopeInt(2)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x].as_long(), 42,
                 "Failed to model `/` operator")


def test_dopeint_operators_comparison():
    x = z3.Int('x')

    solver = z3.Solver()
    solver.add((DopeInt(x) < DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_true(solver.model()[x].as_long() < 42,
                 "Failed to model `<` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) <= DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_true(solver.model()[x].as_long() <= 42,
                 "Failed to model `<=` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) == DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_true(solver.model()[x].as_long() == 42,
                 "Failed to model `==` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) != DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_true(solver.model()[x].as_long() != 42,
                 "Failed to model `!=` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) > DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_true(solver.model()[x].as_long() > 42,
                "Failed to model `>` operator")

    solver = z3.Solver()
    solver.add((DopeInt(x) >= DopeInt(42)).is_true())
    assert solver.check() == z3.sat
    assert_true(solver.model()[x].as_long() >= 42,
                "Failed to model `>=` operator")
