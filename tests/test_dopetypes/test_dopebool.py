import z3
from nose.tools import assert_equal, assert_true

from dopefish.dopetypes import DopeBool


def test_dopebool_variable():
    x = DopeBool.create_z3_variable("x")
    assert_true(z3.is_const(x),
                "Created something that isn't variable")
    assert_true(z3.is_bool(x),
                "Created variable isn't boolean")


def test_dopebool_constraint():
    x = z3.Bool('x')

    solver = z3.Solver()
    solver.add(DopeBool(x).get_constraint(True))
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model True constraint")

    solver = z3.Solver()
    solver.add(DopeBool(x).get_constraint(False))
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model False constraint")


def test_dopebool_is_constraints():
    x = z3.Bool('x')

    solver = z3.Solver()
    solver.add(DopeBool(x).is_true() == True)
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model `is_true()`")

    solver = z3.Solver()
    solver.add(DopeBool(x).is_false() == True)
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `is_false()`")


def test_dopebool_operator_eq():
    x = z3.Bool('x')

    solver = z3.Solver()
    solver.add((DopeBool(x) == DopeBool(True)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model `==` operator (True case)")

    solver = z3.Solver()
    solver.add((DopeBool(x) == DopeBool(False)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `==` operator (False case)")

    solver = z3.Solver()
    solver.add(z3.ForAll([x], (DopeBool(x) == DopeBool(x)).is_true()))
    assert_equal(solver.check(), z3.sat,
                 "`==` operator is not reflexive")


def test_dopebool_operator_ne():
    x = z3.Bool('x')

    solver = z3.Solver()
    solver.add((DopeBool(x) != DopeBool(True)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `==` operator (True case)")

    solver = z3.Solver()
    solver.add((DopeBool(x) != DopeBool(False)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model `==` operator (False case)")

    solver = z3.Solver()
    solver.add((DopeBool(x) != DopeBool(x)).is_true())
    assert_equal(solver.check(), z3.unsat,
                 "`!=` operator is not anti-reflexive")


def test_dopebool_operator_not():
    x = z3.Bool('x')

    solver = z3.Solver()
    solver.add((~DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `~`(NOT) operator (True case)")

    solver = z3.Solver()
    solver.add((~DopeBool(x)).is_false())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model `==` operator (False case)")

    solver = z3.Solver()
    solver.add((~~DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model `~`(NOT) operator (double negation)")


def test_dopebool_operator_and():
    x = z3.Bool('x')
    y = z3.Bool('y')

    solver = z3.Solver()
    solver.add((DopeBool(False) & DopeBool(False) == DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `&`(AND) operator (False & False)")

    solver = z3.Solver()
    solver.add((DopeBool(False) & DopeBool(True) == DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `&`(AND) operator (False & True)")

    solver = z3.Solver()
    solver.add((DopeBool(True) & DopeBool(False) == DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `&`(AND) operator (True & False)")

    solver = z3.Solver()
    solver.add((DopeBool(True) & DopeBool(True) == DopeBool(x)).is_true())
    assert solver.check()
    assert_equal(solver.model()[x], True,
                 "Failed to model `&`(AND) operator (True & True)")

    solver = z3.Solver()
    solver.add(((DopeBool(x) & DopeBool(y)) == (DopeBool(y) & DopeBool(x))).is_true())
    assert_equal(solver.check(), z3.sat,
                "`&`(AND) operator is not commutative")


def test_dopebool_operator_or():
    x = z3.Bool('x')
    y = z3.Bool('y')

    solver = z3.Solver()
    solver.add((DopeBool(False) | DopeBool(False) == DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], False,
                 "Failed to model `|`(OR) operator (False & False)")

    solver = z3.Solver()
    solver.add((DopeBool(False) | DopeBool(True) == DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model `|`(OR) operator (False & True)")

    solver = z3.Solver()
    solver.add((DopeBool(True) | DopeBool(False) == DopeBool(x)).is_true())
    assert solver.check() == z3.sat
    assert_equal(solver.model()[x], True,
                 "Failed to model `|`(OR) operator (True & False)")

    solver = z3.Solver()
    solver.add((DopeBool(True) | DopeBool(True) == DopeBool(x)).is_true())
    assert solver.check()
    assert_equal(solver.model()[x], True,
                 "Failed to model `|`(OR) operator (True & True)")

    solver = z3.Solver()
    solver.add(((DopeBool(x) | DopeBool(y)) == (DopeBool(y) | DopeBool(x))).is_true())
    assert_equal(solver.check(), z3.sat,
                "`|`(OR) operator is not commutative")