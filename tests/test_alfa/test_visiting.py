from nose.tools import assert_equals, assert_in, assert_is_instance

from dopefish import constraints, ConstraintsFactory, dopetypes

interfaces = """
    interface Entity {
      abstract id: str;
    }
    
    [angine=entity]
    interface UrlEntity <: Entity {
      path: str;
      level: int;
      tags: [str];
    }
    
    [angine=subject]
    interface Subject <: Entity {
      name: str;
      roles: [str];
      level: int;
      tags: [str];
      is_admin: bool;
      abstract ip: str;
    }
"""


def test_visitor_simple():
    policy = """
    namespace example {
        policy getStats {
            target clause action == "GET"
            apply permitUnlessDeny
            rule r13 {
                deny
                target clause subject.is_admin != true
            }
        }
    }
    """
    res = ConstraintsFactory.build_tree(
        alfa_policy=policy,
        idl_interfaces=interfaces
    )
    assert_in("subject.is_admin", res.attributes)

    assert_is_instance(res.policies[0], constraints.Policy)
    assert_equals(res.policies[0].name, "getStats")

    assert_equals(len(res.policies[0].targets), 1)
    assert_is_instance(res.policies[0].targets[0], constraints.Expression)

    assert_is_instance(res.policies[0].targets[0].left, constraints.LiteralAttribute)
    assert_is_instance(res.policies[0].targets[0].left.evaluate(), dopetypes.DopeString)
    assert_equals(res.policies[0].targets[0].operator, constraints.Operator.EQ)
    assert_is_instance(res.policies[0].targets[0].right, constraints.LiteralString)
    assert_is_instance(res.policies[0].targets[0].right.evaluate(), dopetypes.DopeString)

    assert_equals(res.policies[0].algorithm, constraints.PermitUnlessDeny)

    assert_equals(len(res.policies[0].rules), 1)
    assert_equals(res.policies[0].rules[0].name, "r13")
    assert_equals(res.policies[0].rules[0].effect, constraints.Effect.DENY)
    assert_equals(len(res.policies[0].rules[0].targets), 1)

    assert_is_instance(res.policies[0].rules[0].targets[0], constraints.Expression)
    assert_is_instance(res.policies[0].rules[0].targets[0].left, constraints.LiteralAttribute)
    assert_is_instance(res.policies[0].rules[0].targets[0].left.evaluate(), dopetypes.DopeBool)
    assert_equals(res.policies[0].rules[0].targets[0].operator, constraints.Operator.NEQ)
    assert_is_instance(res.policies[0].rules[0].targets[0].right, constraints.LiteralBool)
    assert_is_instance(res.policies[0].rules[0].targets[0].right.evaluate(), dopetypes.DopeBool)


def test_visitor_multiple_rules():
    policy = """
    namespace example {
        policy getStats {
            target clause action == "GET" and "/stats" == entity.path
            apply denyUnlessPermit
            rule r11 {
                permit
                target clause subject.name == "b@ckd00r"
            }
            rule r12 {
                permit
                target clause subject.level > entity.level
            }
        }
    }
    """
    res = ConstraintsFactory.build_tree(
        alfa_policy=policy,
        idl_interfaces=interfaces
    )
    assert_in("entity.path", res.attributes)
    assert_in("entity.level", res.attributes)
    assert_in("subject.name", res.attributes)
    assert_in("subject.level", res.attributes)

    assert_equals(len(res.policies[0].targets), 1)
    assert_is_instance(res.policies[0].targets[0].left, constraints.Expression)
    assert_equals(res.policies[0].targets[0].operator, constraints.Operator.AND)
    assert_is_instance(res.policies[0].targets[0].right, constraints.Expression)

    assert_is_instance(res.policies[0].targets[0].left.left, constraints.LiteralAttribute)
    assert_equals(res.policies[0].targets[0].left.operator, constraints.Operator.EQ)
    assert_is_instance(res.policies[0].targets[0].left.right, constraints.LiteralString)
    assert_is_instance(res.policies[0].targets[0].left.evaluate(), dopetypes.DopeBool)


    assert_is_instance(res.policies[0].targets[0].right.left, constraints.LiteralString)
    assert_equals(res.policies[0].targets[0].right.operator, constraints.Operator.EQ)
    assert_is_instance(res.policies[0].targets[0].right.right, constraints.LiteralAttribute)
    assert_is_instance(res.policies[0].targets[0].right.evaluate(), dopetypes.DopeBool)

    assert_equals(res.policies[0].algorithm, constraints.DenyUnlessPermit)
    assert_equals(len(res.policies[0].rules), 2)

    assert_equals(res.policies[0].rules[0].name, "r11")
    assert_equals(res.policies[0].rules[0].effect, constraints.Effect.PERMIT)
    assert_equals(len(res.policies[0].rules[0].targets), 1)

    assert_is_instance(res.policies[0].rules[0].targets[0], constraints.Expression)
    assert_is_instance(res.policies[0].rules[0].targets[0].left, constraints.LiteralAttribute)
    assert_is_instance(res.policies[0].rules[0].targets[0].left.evaluate(), dopetypes.DopeString)
    assert_equals(res.policies[0].rules[0].targets[0].operator, constraints.Operator.EQ)
    assert_is_instance(res.policies[0].rules[0].targets[0].right, constraints.LiteralString)
    assert_is_instance(res.policies[0].rules[0].targets[0].right.evaluate(), dopetypes.DopeString)

    assert_equals(res.policies[0].rules[1].name, "r12")
    assert_equals(res.policies[0].rules[1].effect, constraints.Effect.PERMIT)
    assert_equals(len(res.policies[0].rules[1].targets), 1)

    assert_is_instance(res.policies[0].rules[1].targets[0], constraints.Expression)
    assert_is_instance(res.policies[0].rules[1].targets[0].left, constraints.LiteralAttribute)
    assert_is_instance(res.policies[0].rules[1].targets[0].left.evaluate(), dopetypes.DopeInt)
    assert_equals(res.policies[0].rules[1].targets[0].operator, constraints.Operator.GT)
    assert_is_instance(res.policies[0].rules[1].targets[0].left, constraints.LiteralAttribute)
    assert_is_instance(res.policies[0].rules[1].targets[0].right.evaluate(), dopetypes.DopeInt)


def test_visitor_multiple_policies():
    policy = """
    namespace example {
        export policySet mainPolicy {
            apply denyUnlessPermit
            policySet motd {
                target clause entity.path == "/motd"
                apply denyUnlessPermit
                policy getMotd {
                    target clause action == "GET"
                    apply denyUnlessPermit
                    rule r11 {
                        permit
                        target clause any ["user", "admin"] in subject.roles 
                    }
                }
                policy postMotd {
                    target clause action == "POST"
                    apply denyUnlessPermit
                    rule r12 {
                        permit
                        target clause "admin" in subject.roles
                    }
                }
            }
            policySet debug {
                target clause entity.path in ["/stats", "/api"]
                apply denyUnlessPermit
                policy get {
                    target clause action == "GET"
                    apply denyUnlessPermit
                    rule r13 {
                        permit
                        target clause "admin" in subject.roles
                               clause subject.ip == "127.0.0.1"
                    }
                }
            }
        }
    }
    """
    res = ConstraintsFactory.build_tree(
        alfa_policy=policy,
        idl_interfaces=interfaces
    )

    assert_in("entity.path", res.attributes)
    assert_in("subject.roles", res.attributes)
    assert_in("subject.ip", res.attributes)

    # External policy set
    assert_equals(len(res.policies), 1)
    assert_is_instance(res.policies[0], constraints.PolicySet)
    assert_equals(res.policies[0].name, "mainPolicy")
    assert_equals(len(res.policies[0].policy_sets), 2)
    assert_equals(res.policies[0].algorithm, constraints.DenyUnlessPermit)

    # Policy set "motd"
    policy = res.policies[0].policy_sets[0]
    assert_equals(policy.name, "motd")
    assert_equals(policy.algorithm, constraints.DenyUnlessPermit)

    assert_equals(len(policy.targets), 1)
    assert_is_instance(policy.targets[0].left, constraints.LiteralAttribute)
    assert_equals(policy.targets[0].operator, constraints.Operator.EQ)
    assert_is_instance(policy.targets[0].right, constraints.LiteralString)

    assert_equals(len(policy.policies), 2)
    p1, p2 = policy.policies

    assert_equals(p1.name, "getMotd")
    assert_equals(p1.algorithm, constraints.DenyUnlessPermit)
    assert_equals(len(p1.targets), 1)
    assert_equals(len(p1.rules), 1)

    assert_equals(p1.rules[0].name, "r11")
    assert_equals(p1.rules[0].effect, constraints.Effect.PERMIT)
    assert_equals(len(p1.rules[0].targets), 1)
    assert_is_instance(p1.rules[0].targets[0].left, constraints.LiteralArray)
    assert_equals(p1.rules[0].targets[0].operator, constraints.Operator.INTERSECTS)
    assert_is_instance(p1.rules[0].targets[0].right, constraints.LiteralAttribute)

    assert_equals(p2.name, "postMotd")
    assert_equals(p2.algorithm, constraints.DenyUnlessPermit)
    assert_equals(p2.algorithm, constraints.DenyUnlessPermit)
    assert_equals(len(p2.targets), 1)
    assert_equals(len(p2.rules), 1)

    assert_equals(p2.rules[0].name, "r12")
    assert_equals(p2.rules[0].effect, constraints.Effect.PERMIT)
    assert_equals(len(p2.rules[0].targets), 1)
    assert_is_instance(p2.rules[0].targets[0].left, constraints.LiteralString)
    assert_equals(p2.rules[0].targets[0].operator, constraints.Operator.IN)
    assert_is_instance(p2.rules[0].targets[0].right, constraints.LiteralAttribute)

    # Policy set "debug"
    policy = res.policies[0].policy_sets[1]
    assert_equals(policy.name, "debug")
    assert_equals(policy.algorithm, constraints.DenyUnlessPermit)

    assert_equals(len(policy.targets), 1)
    assert_is_instance(policy.targets[0].left, constraints.LiteralAttribute)
    assert_equals(policy.targets[0].operator, constraints.Operator.IN)
    assert_is_instance(policy.targets[0].right, constraints.LiteralArray)

    assert_equals(len(policy.policies), 1)
    p1 = policy.policies[0]

    assert_equals(p1.name, "get")
    assert_equals(p1.algorithm, constraints.DenyUnlessPermit)
    assert_equals(len(p1.targets), 1)
    assert_equals(len(p1.rules), 1)

    assert_equals(p1.rules[0].name, "r13")
    assert_equals(p1.rules[0].effect, constraints.Effect.PERMIT)
    assert_equals(len(p1.rules[0].targets), 2)

    assert_is_instance(p1.rules[0].targets[0].left, constraints.LiteralString)
    assert_equals(p1.rules[0].targets[0].operator, constraints.Operator.IN)
    assert_is_instance(p1.rules[0].targets[0].right, constraints.LiteralAttribute)

    assert_is_instance(p1.rules[0].targets[1].left, constraints.LiteralAttribute)
    assert_equals(p1.rules[0].targets[1].operator, constraints.Operator.EQ)
    assert_is_instance(p1.rules[0].targets[1].right, constraints.LiteralString)



