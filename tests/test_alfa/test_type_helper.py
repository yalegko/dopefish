import aule
from aule.codegen import ust
from nose.tools import assert_is_none, assert_equals, assert_raises

from dopefish.alfa.type_helper import TypeHelper
from dopefish.constraints import LiteralType


def test_extract_simple_nullable():
    t = TypeHelper._extract_simple_nullable(
        ust.UnionType(
            [ust.TypeReference(ust.SimpleType.BOOLEAN)] # Wrongly constructed union
        )
    )
    assert_is_none(t, "Extracted type from incorrect type")

    t = TypeHelper._extract_simple_nullable(
        ust.UnionType(
            [
                ust.TypeReference(ust.SimpleType.BOOLEAN),
                ust.TypeReference(ust.Identifier("IKeker"))
            ]  # Not nullable union
        )
    )
    assert_is_none(t, "Failed to detect non-nullable type union")

    t = TypeHelper._extract_simple_nullable(
        ust.UnionType(
            [
                ust.TypeReference(ust.SimpleType.BOOLEAN),
                ust.TypeReference(ust.Identifier("IKeker")),
                ust.TypeReference(ust.SimpleType.NULL)
            ]  # Not trivial union
        )
    )
    assert_is_none(t, "Extracted type from non-trivial nullable union")

    t = TypeHelper._extract_simple_nullable(
        ust.UnionType(
            [
                ust.TypeReference(ust.Identifier("IKeker")),
                ust.TypeReference(ust.SimpleType.NULL)
            ]  # Custom nullable type
        )
    )
    assert_is_none(t, "Extracted non-trivial type from nullable union")

    t = TypeHelper._extract_simple_nullable(
        ust.UnionType(
            [
                ust.TypeReference(ust.SimpleType.NULL),
                ust.TypeReference(ust.SimpleType.INTEGER)
            ]  # Simple nullable int
        )
    )
    assert_equals(t, ust.SimpleType.INTEGER,
                  "Extracted unexpected type from Null | Int")

    t = TypeHelper._extract_simple_nullable(
        ust.UnionType(
            [
                ust.TypeReference(ust.SimpleType.INTEGER),
                ust.TypeReference(ust.SimpleType.NULL)
            ]  # Simple nullable int
        )
    )
    assert_equals(t, ust.SimpleType.INTEGER,
                  "Extracted unexpected type from Int | Null")


def test__extract_type():
    t = TypeHelper._extract_type(
        ust.TypeReference(
            ust.SimpleType.BOOLEAN
        )  # Just well simple bool
    )
    assert_equals(t, LiteralType.BOOL,
                  "Extracted unexpected type from simple Bool type reference")

    t = TypeHelper._extract_type(
        ust.TypeReference(
            ust.UnionType([
                ust.TypeReference(ust.SimpleType.INTEGER),
                ust.TypeReference(ust.SimpleType.NULL)
            ])
        )  # Simple nullable int
    )
    assert_equals(t, LiteralType.INT,
                  "Extracted unexpected type from Int | Null")

    t = TypeHelper._extract_type(
        ust.TypeReference(
            ust.SimpleType.INTEGER,
            is_sequence=True
        )  # List of integers
    )
    assert_equals(t, LiteralType.INTSET,
                  "Extracted unexpected type from list of simple types")

    t = TypeHelper._extract_type(
        ust.TypeReference(
            ust.UnionType([
                ust.TypeReference(ust.SimpleType.INTEGER),
                ust.TypeReference(ust.SimpleType.NULL)
            ]),
            is_sequence=True
        )  # Nullable list of integers
    )
    assert_equals(t, LiteralType.INTSET,
                  "Extracted unexpected type from list of nullable simple types")

    def unsupported_type(shit):
        TypeHelper._extract_type(
            ust.TypeReference(ust.Identifier("IKeker")),
        )
    assert_raises(TypeError, unsupported_type,
                  "Failed to detect unsupported type BEFORE extraction")

    def unsupported_simple_type(shit):
        TypeHelper._extract_type(
            ust.TypeReference(
                ust.SimpleType.FLOAT,
            )  # No floats at all
        )
    assert_raises(TypeError, unsupported_simple_type,
                  "Failed to detect unsupported type AFTER extraction")

    def unsupported_simple_type(shit):
        TypeHelper._extract_type(
            ust.TypeReference(
                ust.UnionType([
                    ust.TypeReference(ust.SimpleType.FLOAT),
                    ust.TypeReference(ust.SimpleType.NULL)
                ])
            )  # Wrong nullable
        )
    assert_raises(TypeError, unsupported_simple_type,
                  "Failed to detect unsupported type AFTER extraction from union")

    def unsupported_sequence_type(shit):
        TypeHelper._extract_type(
            ust.TypeReference(
                ust.SimpleType.BOOLEAN,
                is_sequence=True,
            )  # Does not have boolean arrays
        )
    assert_raises(TypeError, unsupported_sequence_type,
                  "Failed to detect unsupported type AFTER extraction from sequence")


def test_typehelper_construction():
    classes = [
        ust.ClassDeclaration(
            ust.Identifier("A"), annotations=[ust.ClassAnnotation("angine=entity")]),
        ust.ClassDeclaration(
            ust.Identifier("B"), annotations=[ust.ClassAnnotation("angine=subject")]),
    ]
    helper = TypeHelper(classes)
    assert_equals(helper.entities['entity'].name, ust.Identifier("A"),
                  "Failed to store entity class")
    assert_equals(helper.entities['subject'].name, ust.Identifier("B"),
                  "Failed to store subject class")

    classes = [
        ust.ClassDeclaration(
            ust.Identifier("A"), annotations=[ust.ClassAnnotation("angine=entity")]),
        ust.ClassDeclaration(
            ust.Identifier("D"), annotations=[ust.ClassAnnotation("angine=unsupported")]),
        ust.ClassDeclaration(
            ust.Identifier("B"), annotations=[ust.ClassAnnotation("angine=subject")]),
        ust.ClassDeclaration(
            ust.Identifier("X")),
    ]
    helper = TypeHelper(classes)
    assert_equals(helper.entities['entity'].name, ust.Identifier("A"),
                  "Failed to store entity class")
    assert_equals(helper.entities['subject'].name, ust.Identifier("B"),
                  "Failed to store subject class")

    def no_subject(shit):
        classes = [
            ust.ClassDeclaration(
                ust.Identifier("A"), annotations=[ust.ClassAnnotation("angine=entity")]),
        ]
        TypeHelper(classes)
    assert_raises(ValueError, no_subject,
                  "Failed to raise exception w/o subject class")

    def no_entity(shit):
        classes = [
            ust.ClassDeclaration(
                ust.Identifier("B"), annotations=[ust.ClassAnnotation("angine=subject")]),
        ]
        TypeHelper(classes)
    assert_raises(ValueError, no_entity,
                  "Failed to raise exception w/o entity class")


def test_resolve_type():
    idl = """
    [angine=entity]
    interface UrlEntity {
      abstract id: str;
      secret: bool;
      level: int;
      tags: [int];
    }
    
    [angine=subject]
    interface Subject {
      abstract id: str;
      roles: [str];
      level: int;
      abstract ip: str;
    }
    """
    interfaces = aule.ASTParserFactory \
        .create("idl") \
        .parse(idl)
    type_helper=TypeHelper(interfaces)

    tests = [
        ("entity.id", LiteralType.STRING),
        ("entity.secret", LiteralType.BOOL),
        ("entity.level", LiteralType.INT),
        ("entity.tags", LiteralType.INTSET),
        ("subject.id", LiteralType.STRING),
        ("subject.roles", LiteralType.STRINGSET),
        ("subject.level", LiteralType.INT),
        ("subject.ip", LiteralType.STRING),
    ]

    for access, result in tests:
        assert_equals(type_helper.resolve_type(access), result,
                      "Failed to resolve proper type of `{}`".format(access))

    def wrong_access(shit):
        type_helper.resolve_type("entity.roles.0")
    assert_raises(ValueError, wrong_access, "Failed detect improper access string")

    def no_access(shit):
        type_helper.resolve_type("entity")
    assert_raises(ValueError, no_access, "Failed detect absence of access")

    def unknown_entity(shit):
        type_helper.resolve_type("mickie.mouse")
    assert_raises(ValueError, unknown_entity, "Failed detect unknown entity type")

    def unknown_attribute(shit):
        type_helper.resolve_type("subject.superpower")
    assert_raises(ValueError, unknown_attribute, "Failed detect unknown attribute")