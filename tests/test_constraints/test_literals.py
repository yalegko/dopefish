import z3
from nose.tools import assert_true, assert_equal, assert_raises, assert_greater

from dopefish.constraints import *
from dopefish.dopetypes import *
from dopefish.dopetypes.dopeset import DopeStringSet


def test_literals_basic():
    literal = LiteralBool(True)
    assert_true(isinstance(literal.evaluate(), DopeBool),
                "LiteralBool evaluated to unexpected type")
    assert_equal(literal.evaluate().inner_z3var, True,
                "Failed to pass LiteralBool value to the constraint")

    literal = LiteralInt(42)
    assert_true(isinstance(literal.evaluate(), DopeInt),
                "LiteralInt evaluated to unexpected type")
    assert_equal(literal.evaluate().inner_z3var, 42,
                "Failed to pass LiteralInt value to the constraint")

    literal = LiteralString("kekulechek")
    assert_true(isinstance(literal.evaluate(), DopeString),
                "LiteralString evaluated to unexpected type")
    assert_equal(literal.evaluate().inner_z3var, "kekulechek",
                "Failed to pass LiteralString value to the constraint")


def test_literals_array():
    literal = LiteralArray([1, 2, 3])
    assert_true(isinstance(literal.evaluate(), DopeIntSet),
                "LiteralArray of ints evaluated to unexpected type")
    assert_true(z3.is_array(literal.evaluate().inner_z3var),
                "Failed to pass LiteralArray value to the constraint")

    literal = LiteralArray(["lol", "kek", "cheburek"])
    assert_true(isinstance(literal.evaluate(), DopeStringSet),
                "LiteralArray of strings evaluated to unexpected type")
    assert_true(z3.is_array(literal.evaluate().inner_z3var),
                "Failed to pass LiteralArray value to the constraint")

    def invalid_array_type(shit):
        LiteralArray([0.7, 0.77, 0.777])
    assert_raises(ValueError, invalid_array_type,
                  "LiteralArray failed to detect invalid array type")

    def mixed_array(shit):
        LiteralArray([1, "kek", 42])
    assert_raises(ValueError, mixed_array,
                  "LiteralArray failed to detect mixed array")

    literal = LiteralArray(["net chebureka"])
    assert_greater(len(literal.additional_constraints()), 0,
                "LiteralArray of strings evaluated to unexpected type")


def test_literal_attributes():
    tests = [
        ("i", LiteralType.INT, DopeInt),
        ("b", LiteralType.BOOL, DopeBool),
        ("s", LiteralType.STRING, DopeString),
        ("si", LiteralType.INTSET, DopeIntSet),
        ("ss", LiteralType.STRINGSET, DopeStringSet),
    ]
    for name, type_, res_type in tests:
        literal = LiteralAttribute(name, type_)
        assert_true(isinstance(literal.evaluate(), res_type),
                    "LiteralAttribute evaluated to unexpected type from {}".format(type_))

    def mixed_array(shit):
        LiteralAttribute("nope", "don't pass shit here pls, kk?")
    assert_raises(ValueError, mixed_array,
                  "LiteralAttribute failed to detect improper literal type")