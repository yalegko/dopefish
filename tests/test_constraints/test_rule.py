import z3
from nose.tools import assert_is_instance, assert_equal, assert_true, assert_raises

from dopefish.constraints import Effect, Rule
from dopefish.constraints.interfaces import IEvaluable, combine_targets
from dopefish.dopetypes import DopeBool


class EvMock(IEvaluable):
    def __init__(self, bool):
        self.value = bool

    def evaluate(self):
        return self.value


def test_combine_targets():
    expr = combine_targets()
    assert_is_instance(expr, DopeBool,
                       "Got non-boolean result of targets combination")
    assert_equal(expr.inner_z3var, True,
                 "Combination of 0 targets didn't lead to True condition")

    expr = combine_targets(EvMock(DopeBool(False)))
    assert_true(z3.is_false(z3.simplify(expr.is_true())),
                 "Combining single target lead to unexpected result")

    vals = [EvMock(DopeBool(v)) for v in [False, False, True, False]]
    expr = combine_targets(*vals)
    assert_true(z3.is_true(z3.simplify(expr.is_true())),
                 "Combining multiple targets lead to unexpected result")


def test_rule_permit():
    rule = Rule("rule", Effect.PERMIT, targets=[])
    assert_true(z3.is_true(z3.simplify(rule.permit)),
                "Failed to permit rule with True target on PERMIT effect")

    rule = Rule("rule", Effect.DENY, targets=[])
    assert_true(z3.is_false(z3.simplify(rule.permit)),
                "Improperly permit rule with True target on DENY effect")

    def wrong_effect(shit):
         Rule("rule", "omg why", targets=[]).permit()
    assert_raises(ValueError, wrong_effect,
                "Failed to raise exception on unknown effect")


def test_rule_deny():
    rule = Rule("rule", Effect.PERMIT, targets=[])
    assert_true(z3.is_false(z3.simplify(rule.deny)),
                "Failed to permit rule with True target on PERMIT effect")

    rule = Rule("rule", Effect.DENY, targets=[])
    assert_true(z3.is_true(z3.simplify(rule.deny)),
                "Improperly permit rule with True target on DENY effect")

    def wrong_effect(shit):
         Rule("rule", "omg why", targets=[]).deny()
    assert_raises(ValueError, wrong_effect,
                  "Failed to raise exception on unknown effect")


def test_not_applicable():
    rule = Rule("rule", Effect.PERMIT, targets=[])
    assert_true(z3.is_false(z3.simplify(rule.not_applicable)),
                "Marked rule as not applicable with True target")

    rule = Rule("rule", Effect.PERMIT, targets=[EvMock(DopeBool(False))])
    assert_true(z3.is_true(z3.simplify(rule.not_applicable)),
                "Failed to detect not applicable rule with False target")


def test_indeterminate():
    dope = DopeBool(None, is_missing=False, is_err=False)
    rule = Rule("rule", Effect.PERMIT, targets=[EvMock(dope)])
    assert_true(z3.is_false(z3.simplify(rule.indeterminate)),
                "Improperly marked rule as indeterminate")

    dope = DopeBool(None, is_missing=True, is_err=False)
    rule = Rule("rule", Effect.PERMIT, targets=[EvMock(dope)])
    assert_true(z3.is_true(z3.simplify(rule.indeterminate)),
                "Failed to mark rule with missing attribute as indeterminate")

    dope = DopeBool(None, is_missing=False, is_err=True)
    rule = Rule("rule", Effect.PERMIT, targets=[EvMock(dope)])
    assert_true(z3.is_true(z3.simplify(rule.indeterminate)),
                "Failed to mark rule with error as indeterminate")