import z3
from nose.tools import assert_true

from dopefish.constraints import (Policy, PolicySet, FirstApplicable,
                                  Rule, Effect)
from dopefish.dopetypes import DopeBool
from .test_rule import EvMock


def test_policy_decisions():
    policy = Policy("Simple applicable permit policy",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(True))],
                    rules=[Rule("", Effect.PERMIT, [])])
    assert_true(z3.is_true(z3.simplify(policy.permit)))
    assert_true(z3.is_false(z3.simplify(policy.deny)))
    assert_true(z3.is_false(z3.simplify(policy.not_applicable)))
    assert_true(z3.is_false(z3.simplify(policy.indeterminate)))

    policy = Policy("Simple not-applicable policy",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(False))],
                    rules=[Rule("", Effect.PERMIT, [])])
    assert_true(z3.is_false(z3.simplify(policy.permit)))
    assert_true(z3.is_false(z3.simplify(policy.deny)))
    assert_true(z3.is_true(z3.simplify(policy.not_applicable)))
    assert_true(z3.is_false(z3.simplify(policy.indeterminate)))

    policy = Policy("Applicable policy with not-applicable rule",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(True))],
                    rules=[Rule("", Effect.PERMIT, [EvMock(DopeBool(False))])])
    assert_true(z3.is_false(z3.simplify(policy.permit)))
    assert_true(z3.is_false(z3.simplify(policy.deny)))
    assert_true(z3.is_true(z3.simplify(policy.not_applicable)))
    assert_true(z3.is_false(z3.simplify(policy.indeterminate)))

    policy = Policy("Simple indeterminate policy with missing target",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(is_missing=True))],
                    rules=[Rule("", Effect.PERMIT, [])])
    assert_true(z3.is_false(z3.simplify(policy.permit)))
    assert_true(z3.is_false(z3.simplify(policy.deny)))
    assert_true(z3.is_false(z3.simplify(policy.not_applicable)))
    assert_true(z3.is_true(z3.simplify(policy.indeterminate)))

    policy = Policy("Simple indeterminate policy with error target",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(is_err=True))],
                    rules=[Rule("", Effect.PERMIT, [])])
    assert_true(z3.is_false(z3.simplify(policy.permit)))
    assert_true(z3.is_false(z3.simplify(policy.deny)))
    assert_true(z3.is_false(z3.simplify(policy.not_applicable)))
    assert_true(z3.is_true(z3.simplify(policy.indeterminate)))

    policy = Policy("Applicable policy with indeterminate rule",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(True))],
                    rules=[Rule("", Effect.PERMIT, [EvMock(DopeBool(is_err=True))])])
    assert_true(z3.is_false(z3.simplify(policy.permit)))
    assert_true(z3.is_false(z3.simplify(policy.deny)))
    assert_true(z3.is_false(z3.simplify(policy.not_applicable)))
    assert_true(z3.is_true(z3.simplify(policy.indeterminate)))


def test_policy_set_decisions():
    permit_policy = Policy("Simple applicable permit policy",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(True))],
                    rules=[Rule("", Effect.PERMIT, [])])
    not_applicable_policy = Policy("Simple not-applicable policy",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(False))],
                    rules=[Rule("", Effect.PERMIT, [])])
    indeterminate_policy =  Policy("Simple indeterminate policy with missing target",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(is_missing=True))],
                    rules=[Rule("", Effect.PERMIT, [])])

    permit_policy_set = PolicySet("Simple applicable permit policy set",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(True))],
                    policies=[permit_policy],
                    policy_sets=[])
    assert_true(z3.is_true(z3.simplify(permit_policy_set.permit)))
    assert_true(z3.is_false(z3.simplify(permit_policy_set.deny)))
    assert_true(z3.is_false(z3.simplify(permit_policy_set.not_applicable)))
    assert_true(z3.is_false(z3.simplify(permit_policy_set.indeterminate)))

    policy_set = PolicySet("Simple not-applicable policy set",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(False))],
                    policies=[permit_policy],
                    policy_sets=[])
    assert_true(z3.is_false(z3.simplify(policy_set.permit)))
    assert_true(z3.is_false(z3.simplify(policy_set.deny)))
    assert_true(z3.is_true(z3.simplify(policy_set.not_applicable)))
    assert_true(z3.is_false(z3.simplify(policy_set.indeterminate)))

    policy_set = PolicySet("Applicable policy set with not-applicable policy",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(True))],
                    policies=[not_applicable_policy],
                    policy_sets=[])
    assert_true(z3.is_false(z3.simplify(policy_set.permit)))
    assert_true(z3.is_false(z3.simplify(policy_set.deny)))
    assert_true(z3.is_true(z3.simplify(policy_set.not_applicable)))
    assert_true(z3.is_false(z3.simplify(policy_set.indeterminate)))

    policy_set = PolicySet("Simple indeterminate policy set with missing target",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(is_missing=True))],
                    policies=[permit_policy],
                    policy_sets=[])
    assert_true(z3.is_false(z3.simplify(policy_set.permit)))
    assert_true(z3.is_false(z3.simplify(policy_set.deny)))
    assert_true(z3.is_false(z3.simplify(policy_set.not_applicable)))
    assert_true(z3.is_true(z3.simplify(policy_set.indeterminate)))

    policy_set = PolicySet("Simple indeterminate policy set with error target",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(is_err=True))],
                    policies=[permit_policy],
                    policy_sets=[])
    assert_true(z3.is_false(z3.simplify(policy_set.permit)))
    assert_true(z3.is_false(z3.simplify(policy_set.deny)))
    assert_true(z3.is_false(z3.simplify(policy_set.not_applicable)))
    assert_true(z3.is_true(z3.simplify(policy_set.indeterminate)))

    policy_set = PolicySet("Applicable policy set with indeterminate policy",
                    FirstApplicable,
                    targets=[EvMock(DopeBool(True))],
                           policies=[indeterminate_policy],
                           policy_sets=[])
    assert_true(z3.is_false(z3.simplify(policy_set.permit)))
    assert_true(z3.is_false(z3.simplify(policy_set.deny)))
    assert_true(z3.is_false(z3.simplify(policy_set.not_applicable)))
    assert_true(z3.is_true(z3.simplify(policy_set.indeterminate)))

    policy_set = PolicySet("Applicable policy set with not-applicable policy and applicable policy set",
                           FirstApplicable,
                           targets=[EvMock(DopeBool(True))],
                           policies=[not_applicable_policy],
                           policy_sets=[permit_policy_set])
    assert_true(z3.is_true(z3.simplify(policy_set.permit)))
    assert_true(z3.is_false(z3.simplify(policy_set.deny)))
    assert_true(z3.is_false(z3.simplify(policy_set.not_applicable)))
    assert_true(z3.is_false(z3.simplify(policy_set.indeterminate)))