from nose.tools import assert_equals, assert_is_instance, assert_raises

from dopefish.constraints import LiteralInt, LiteralAttribute, LiteralType
from dopefish.constraints.expression import Operator, Expression
from dopefish.constraints.interfaces import IEvaluable
from dopefish.dopetypes import DopeBool


def test_expression_operands():
    # Expected operations map
    operations = {
        "__eq__": Operator.EQ,
        "__ne__": Operator.NEQ,
        "__lt__": Operator.LT,
        "__gt__": Operator.GT,
        "__le__": Operator.LE,
        "__ge__": Operator.GE,
        "__invert__": Operator.NOT,
        "__and__": Operator.AND,
        "__or__": Operator.OR,
        "contains": Operator.IN,
        "issubset": Operator.SUBSET,
        "issubseteq": Operator.SUBSETEQ,
        "intersects": Operator.INTERSECTS,
    }
    # Create evaluable mock that could be used with any of operations above
    class EvaluableMock(IEvaluable):
        def evaluate(self):
            return self
    # Make every operand to return correct operand
    for name, op in operations.items():
        setattr(EvaluableMock, name, lambda s,o: op)
    setattr(EvaluableMock, "__invert__", lambda s: operations["__invert__"])
    # And now test Expression's `.evaluate()`
    for name, op in operations.items():
        expr =  Expression(EvaluableMock(),op,EvaluableMock())
        assert_equals(expr.evaluate(), op,
            "Failed to call {} on {}".format(name, op)
        )


def test_expression_bad_op():
    def bad_op(shit):
        Expression(LiteralInt(42), "==", LiteralInt(42)).evaluate()
    assert_raises(ValueError, bad_op,
                  "Expression failed to detect improper operator")


def test_expression_application():
    expression = Expression(
        LiteralAttribute("subject.num_keks", LiteralType.INT),
        Operator.EQ,
        LiteralInt(42)
    )
    assert_is_instance(expression.evaluate(), DopeBool,
                       "Evaluated simple terminal expression to the wrong type")

    expression = Expression(
        Expression(
            LiteralAttribute("subject.num_keks", LiteralType.INT),
            Operator.LT,
            LiteralInt(42)
        ),
        Operator.NOT,
        None
    )
    assert_is_instance(expression.evaluate(), DopeBool,
                       "Evaluated NOT-expression to the wrong type")

    expression = Expression(
        Expression(
            LiteralAttribute("subject.num_keks", LiteralType.INT),
            Operator.EQ,
            LiteralInt(42)
        ),
        Operator.AND,
        Expression(
            LiteralAttribute("subject.or_level", LiteralType.INT),
            Operator.GT,
            LiteralInt(9000)
        )
    )
    assert_is_instance(expression.evaluate(), DopeBool,
                       "Evaluated AND-expression to the wrong type")

    expression = Expression(
        Expression(
            Expression(
                LiteralAttribute("subject.num_keks", LiteralType.INT),
                Operator.LT,
                LiteralInt(42)
            ),
            Operator.NOT,
            None
        ),
        Operator.AND,
        Expression(
            LiteralAttribute("subject.or_level", LiteralType.INT),
            Operator.GT,
            LiteralInt(9000)
        )
    )
    assert_is_instance(expression.evaluate(), DopeBool,
                       "Evaluated complex expression to the wrong type")