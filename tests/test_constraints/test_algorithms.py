import z3
from nose.tools import assert_true, assert_equal, assert_raises

from dopefish.constraints.algorithms import (
    CombiningResult,
    PermitOverrides, DenyOverrides, DenyUnlessPermit, PermitUnlessDeny,
    FirstApplicable, OnlyOneApplicable
)

"""
    Tests sources https://www.axiomatics.com/blog/understanding-xacml-combining-algorithms/
"""


class CombinableMock(CombiningResult):
    def __init__(self,
                 permit=False, deny=False, not_applicable=False, indeterminate=False):
        super().__init__(permit, deny, indeterminate, not_applicable)


def __is_permit(res: CombiningResult):
    return z3.is_true(z3.simplify(
        z3.And(
            res.permit,
            z3.Not(res.deny),
            z3.Not(res.not_applicable),
            z3.Not(res.indeterminate)
        )
    ))


def __is_deny(res: CombiningResult):
    return z3.is_true(z3.simplify(
        z3.And(
            z3.Not(res.permit),
            res.deny,
            z3.Not(res.not_applicable),
            z3.Not(res.indeterminate)
        )
    ))


def __is_not_applicable(res: CombiningResult):
    return z3.is_true(z3.simplify(
        z3.And(
            z3.Not(res.permit),
            z3.Not(res.deny),
            res.not_applicable,
            z3.Not(res.indeterminate)
        )
    ))


def __is_indeterminate(res: CombiningResult):
    return z3.is_true(z3.simplify(
        z3.And(
            z3.Not(res.permit),
            z3.Not(res.deny),
            z3.Not(res.not_applicable),
            res.indeterminate
        )
    ))


def test_deny_overrides():
    tests = [
        ("Permit x Permit", (dict(permit=True), dict(permit=True)),         __is_permit),
        ("Permit x Deny",   (dict(permit=True), dict(deny=True)),           __is_deny),
        ("Permit x N/A",    (dict(permit=True), dict(not_applicable=True)), __is_permit),
        ("Permit x Indet",  (dict(permit=True), dict(indeterminate=True)),  __is_indeterminate),

        ("Deny x Permit",   (dict(deny=True), dict(permit=True)),           __is_deny),
        ("Deny x Deny",     (dict(deny=True), dict(deny=True)),             __is_deny),
        ("Deny x N/A",      (dict(deny=True), dict(not_applicable=True)),   __is_deny),
        ("Deny x Indet",    (dict(deny=True), dict(indeterminate=True)),    __is_deny),

        ("N/A x Permit",    (dict(not_applicable=True), dict(permit=True)),         __is_permit),
        ("N/A x Deny",      (dict(not_applicable=True), dict(deny=True)),           __is_deny),
        ("N/A x N/A",       (dict(not_applicable=True), dict(not_applicable=True)), __is_not_applicable),
        ("N/A x Indet",     (dict(not_applicable=True), dict(indeterminate=True)),  __is_indeterminate),

        ("Indet x Permit",  (dict(indeterminate=True), dict(permit=True)),          __is_indeterminate),
        ("Indet x Deny",    (dict(indeterminate=True), dict(deny=True)),            __is_deny),
        ("Indet x N/A",     (dict(indeterminate=True), dict(not_applicable=True)),  __is_indeterminate),
        ("Indet x Indet",   (dict(indeterminate=True), dict(indeterminate=True)),   __is_indeterminate)
    ]

    for name, (arg0, arg1), test in tests:
        assert_true(test(DenyOverrides.combine(CombinableMock(**arg0), CombinableMock(**arg1))),
                    "DenyOverrides failed to combine " + name)


def test_permit_overrides():
    tests = [
        ("Permit x Permit", (dict(permit=True), dict(permit=True)),         __is_permit),
        ("Permit x Deny",   (dict(permit=True), dict(deny=True)),           __is_permit),
        ("Permit x N/A",    (dict(permit=True), dict(not_applicable=True)), __is_permit),
        ("Permit x Indet",  (dict(permit=True), dict(indeterminate=True)),  __is_permit),

        ("Deny x Permit",   (dict(deny=True), dict(permit=True)),           __is_permit),
        ("Deny x Deny",     (dict(deny=True), dict(deny=True)),             __is_deny),
        ("Deny x N/A",      (dict(deny=True), dict(not_applicable=True)),   __is_deny),
        ("Deny x Indet",    (dict(deny=True), dict(indeterminate=True)),    __is_indeterminate),

        ("N/A x Permit",    (dict(not_applicable=True), dict(permit=True)),         __is_permit),
        ("N/A x Deny",      (dict(not_applicable=True), dict(deny=True)),           __is_deny),
        ("N/A x N/A",       (dict(not_applicable=True), dict(not_applicable=True)), __is_not_applicable),
        ("N/A x Indet",     (dict(not_applicable=True), dict(indeterminate=True)),  __is_indeterminate),

        ("Indet x Permit",  (dict(indeterminate=True), dict(permit=True)),          __is_permit),
        ("Indet x Deny",    (dict(indeterminate=True), dict(deny=True)),            __is_indeterminate),
        ("Indet x N/A",     (dict(indeterminate=True), dict(not_applicable=True)),  __is_indeterminate),
        ("Indet x Indet",   (dict(indeterminate=True), dict(indeterminate=True)),   __is_indeterminate)
    ]

    for name, (arg0, arg1), test in tests:
        assert_true(test(PermitOverrides.combine(CombinableMock(**arg0), CombinableMock(**arg1))),
                    "PermitOverrides failed to combine " + name)


def test_deny_unless_permit():
    tests = [
        ("Permit x Permit", (dict(permit=True), dict(permit=True)),         __is_permit),
        ("Permit x Deny",   (dict(permit=True), dict(deny=True)),           __is_permit),
        ("Permit x N/A",    (dict(permit=True), dict(not_applicable=True)), __is_permit),
        ("Permit x Indet",  (dict(permit=True), dict(indeterminate=True)),  __is_permit),

        ("Deny x Permit",   (dict(deny=True), dict(permit=True)),           __is_permit),
        ("Deny x Deny",     (dict(deny=True), dict(deny=True)),             __is_deny),
        ("Deny x N/A",      (dict(deny=True), dict(not_applicable=True)),   __is_deny),
        ("Deny x Indet",    (dict(deny=True), dict(indeterminate=True)),    __is_deny),

        ("N/A x Permit",    (dict(not_applicable=True), dict(permit=True)),         __is_permit),
        ("N/A x Deny",      (dict(not_applicable=True), dict(deny=True)),           __is_deny),
        ("N/A x N/A",       (dict(not_applicable=True), dict(not_applicable=True)), __is_deny),
        ("N/A x Indet",     (dict(not_applicable=True), dict(indeterminate=True)),  __is_deny),

        ("Indet x Permit",  (dict(indeterminate=True), dict(permit=True)),          __is_permit),
        ("Indet x Deny",    (dict(indeterminate=True), dict(deny=True)),            __is_deny),
        ("Indet x N/A",     (dict(indeterminate=True), dict(not_applicable=True)),  __is_deny),
        ("Indet x Indet",   (dict(indeterminate=True), dict(indeterminate=True)),   __is_deny)
    ]

    for name, (arg0, arg1), test in tests:
        assert_true(test(DenyUnlessPermit.combine(CombinableMock(**arg0), CombinableMock(**arg1))),
                    "DenyUnlessPermit failed to combine " + name)


def test_permit_unless_deny():
    tests = [
        ("Permit x Permit", (dict(permit=True), dict(permit=True)),         __is_permit),
        ("Permit x Deny",   (dict(permit=True), dict(deny=True)),           __is_deny),
        ("Permit x N/A",    (dict(permit=True), dict(not_applicable=True)), __is_permit),
        ("Permit x Indet",  (dict(permit=True), dict(indeterminate=True)),  __is_permit),

        ("Deny x Permit",   (dict(deny=True), dict(permit=True)),           __is_deny),
        ("Deny x Deny",     (dict(deny=True), dict(deny=True)),             __is_deny),
        ("Deny x N/A",      (dict(deny=True), dict(not_applicable=True)),   __is_deny),
        ("Deny x Indet",    (dict(deny=True), dict(indeterminate=True)),    __is_deny),

        ("N/A x Permit",    (dict(not_applicable=True), dict(permit=True)),         __is_permit),
        ("N/A x Deny",      (dict(not_applicable=True), dict(deny=True)),           __is_deny),
        ("N/A x N/A",       (dict(not_applicable=True), dict(not_applicable=True)), __is_permit),
        ("N/A x Indet",     (dict(not_applicable=True), dict(indeterminate=True)),  __is_permit),

        ("Indet x Permit",  (dict(indeterminate=True), dict(permit=True)),          __is_permit),
        ("Indet x Deny",    (dict(indeterminate=True), dict(deny=True)),            __is_deny),
        ("Indet x N/A",     (dict(indeterminate=True), dict(not_applicable=True)),  __is_permit),
        ("Indet x Indet",   (dict(indeterminate=True), dict(indeterminate=True)),   __is_permit)
    ]

    for name, (arg0, arg1), test in tests:
        assert_true(test(PermitUnlessDeny.combine(CombinableMock(**arg0), CombinableMock(**arg1))),
                    "PermitUnlessDeny failed to combine " + name)


def test_first_applicable():
    tests = [
        ("Permit x Permit", (dict(permit=True), dict(permit=True)),         __is_permit),
        ("Permit x Deny",   (dict(permit=True), dict(deny=True)),           __is_permit),
        ("Permit x N/A",    (dict(permit=True), dict(not_applicable=True)), __is_permit),
        ("Permit x Indet",  (dict(permit=True), dict(indeterminate=True)),  __is_permit),

        ("Deny x Permit",   (dict(deny=True), dict(permit=True)),           __is_deny),
        ("Deny x Deny",     (dict(deny=True), dict(deny=True)),             __is_deny),
        ("Deny x N/A",      (dict(deny=True), dict(not_applicable=True)),   __is_deny),
        ("Deny x Indet",    (dict(deny=True), dict(indeterminate=True)),    __is_deny),

        ("N/A x Permit",    (dict(not_applicable=True), dict(permit=True)),         __is_permit),
        ("N/A x Deny",      (dict(not_applicable=True), dict(deny=True)),           __is_deny),
        ("N/A x N/A",       (dict(not_applicable=True), dict(not_applicable=True)), __is_not_applicable),
        ("N/A x Indet",     (dict(not_applicable=True), dict(indeterminate=True)),  __is_indeterminate),

        ("Indet x Permit",  (dict(indeterminate=True), dict(permit=True)),          __is_indeterminate),
        ("Indet x Deny",    (dict(indeterminate=True), dict(deny=True)),            __is_indeterminate),
        ("Indet x N/A",     (dict(indeterminate=True), dict(not_applicable=True)),  __is_indeterminate),
        ("Indet x Indet",   (dict(indeterminate=True), dict(indeterminate=True)),   __is_indeterminate)
    ]

    for name, (arg0, arg1), test in tests:
        assert_true(test(FirstApplicable.combine(CombinableMock(**arg0), CombinableMock(**arg1))),
                    "FirstApplicable failed to combine " + name)


def test_only_one_applicable():
    tests = [
        ("Permit x Permit", (dict(permit=True), dict(permit=True)),         __is_indeterminate),
        ("Permit x Deny",   (dict(permit=True), dict(deny=True)),           __is_indeterminate),
        ("Permit x N/A",    (dict(permit=True), dict(not_applicable=True)), __is_permit),
        ("Permit x Indet",  (dict(permit=True), dict(indeterminate=True)),  __is_indeterminate),

        ("Deny x Permit",   (dict(deny=True), dict(permit=True)),           __is_indeterminate),
        ("Deny x Deny",     (dict(deny=True), dict(deny=True)),             __is_indeterminate),
        ("Deny x N/A",      (dict(deny=True), dict(not_applicable=True)),   __is_deny),
        ("Deny x Indet",    (dict(deny=True), dict(indeterminate=True)),    __is_indeterminate),

        ("N/A x Permit",    (dict(not_applicable=True), dict(permit=True)),         __is_permit),
        ("N/A x Deny",      (dict(not_applicable=True), dict(deny=True)),           __is_deny),
        ("N/A x N/A",       (dict(not_applicable=True), dict(not_applicable=True)), __is_not_applicable),
        ("N/A x Indet",     (dict(not_applicable=True), dict(indeterminate=True)),  __is_indeterminate),

        ("Indet x Permit",  (dict(indeterminate=True), dict(permit=True)),          __is_indeterminate),
        ("Indet x Deny",    (dict(indeterminate=True), dict(deny=True)),            __is_indeterminate),
        ("Indet x N/A",     (dict(indeterminate=True), dict(not_applicable=True)),  __is_indeterminate),
        ("Indet x Indet",   (dict(indeterminate=True), dict(indeterminate=True)),   __is_indeterminate)
    ]

    for name, (arg0, arg1), test in tests:
        assert_true(test(OnlyOneApplicable.combine(CombinableMock(**arg0), CombinableMock(**arg1))),
                    "OnlyOneApplicable failed to combine " + name)


def test_single_deny_unless_permit():
    tests = [
        ("Permit", dict(permit=True),          __is_permit),
        ("Deny",   dict(deny=True),            __is_deny),
        ("N/A",    dict(not_applicable=True),  __is_deny),
        ("Indet",  dict(indeterminate=True),   __is_deny),
    ]

    for name, arg, test in tests:
        assert_true(test(DenyUnlessPermit.combine(CombinableMock(**arg))),
                    "DenyUnlessPermit failed to process single " + name)


def test_single_permit_unless_deny():
    tests = [
        ("Permit", dict(permit=True),          __is_permit),
        ("Deny",   dict(deny=True),            __is_deny),
        ("N/A",    dict(not_applicable=True),  __is_permit),
        ("Indet",  dict(indeterminate=True),   __is_permit),
    ]

    for name, arg, test in tests:
        assert_true(test(PermitUnlessDeny.combine(CombinableMock(**arg))),
                    "PermitUnlessDeny failed to process single " + name)


def test_edge_cases():
    arg = CombinableMock(permit=True, deny=True, not_applicable=True, indeterminate=True)
    any_algo = FirstApplicable
    res = any_algo.combine(arg)

    assert_equal(res.permit, arg.permit,
                 "Lost permit value of the single argument")
    assert_equal(res.deny, arg.deny,
                 "Lost deny value of the single argument")
    assert_equal(res.indeterminate, arg.indeterminate,
                 "Lost indeterminate value of the single argument")
    assert_equal(res.not_applicable, arg.not_applicable,
                 "Lost not_applicable value of the single argument")

    res = None
    def combine_nothing(shit):
        res = any_algo.combine()
    assert_raises(ValueError, combine_nothing,
                  "Combined no arguments into {}".format(res))
