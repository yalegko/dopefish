from setuptools import setup, find_packages

setup(
    name="dopefish",
    version=0.1,
    description="ALFA properties checker based on Microsoft z3",
    author="yalegko",
    author_email="yalegko@gmail.com",
    license="GPL",
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=open("requirements.txt").readlines(),
)