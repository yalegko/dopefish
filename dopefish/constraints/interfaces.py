from abc import ABC, abstractmethod
from functools import reduce
from typing import Union

from dopefish.dopetypes import DopeBool, DopeBaseSet, DopeString, DopeInt


class ICombinable(ABC): # pragma: no cover
    @property
    @abstractmethod
    def permit(self):
        pass

    @property
    @abstractmethod
    def deny(self):
        pass

    @property
    @abstractmethod
    def not_applicable(self):
        pass

    @property
    @abstractmethod
    def indeterminate(self):
        pass


class IEvaluable(ABC): # pragma: no cover
    @abstractmethod
    def evaluate(self) -> Union[DopeString, DopeInt, DopeBool, DopeBaseSet]:
        pass


def combine_targets(*targets: IEvaluable) -> DopeBool:
        """ Currently combines expressions using extended OR (DopeBool | DopeBool) """
        if len(targets) < 1:
            return DopeBool(True)
        if len(targets) == 1:
            # that's the same result as below, but more simple for smt
            return targets[0].evaluate()
        return reduce(
            lambda x, y: x | y.evaluate(),
            targets,
            DopeBool(False), # neutral to logical OR
        )