from enum import Enum

from dopefish.constraints.interfaces import IEvaluable
from dopefish.dopetypes import DopeBool, DopeInt, DopeString
from dopefish.dopetypes import DopeIntSet, DopeStringSet
from dopefish.dopetypes.dopebase import DopeBase


class LiteralType(Enum):
    INT = "int"
    BOOL = "bool"
    STRING = "str"
    INTSET = "intset"
    STRINGSET = "stringset"


class AbstractLiteral(IEvaluable):
    def __init__(self, value):
        self.expression = value

    def evaluate(self) -> DopeBase:
        return self.expression


class LiteralBool(AbstractLiteral):
    def __init__(self, value):
        super().__init__(DopeBool(value))


class LiteralInt(AbstractLiteral):
    def __init__(self, value):
        super().__init__(DopeInt(value))


class LiteralString(AbstractLiteral):
    def __init__(self, value):
        super().__init__(DopeString(value))


class LiteralArray(AbstractLiteral):
    def __init__(self, values):
        if not all(isinstance(x, type(values[0])) for x in values):
            raise ValueError("All elements types should be the same; got {}".format(values))
        if isinstance(values[0], str):
            z3obj = DopeStringSet.from_values(values)
        elif isinstance(values[0], int):
            z3obj = DopeIntSet.from_values(values)
        else:
            raise ValueError("Unsupported array element type; got {}".format(values))
        super().__init__(z3obj)

    def additional_constraints(self):
        return self.expression.additional_constraints


class LiteralAttribute(AbstractLiteral):
    def __init__(self, name, type):
        if type == LiteralType.INT:
            dopevar = DopeInt.create_variable(name)
        elif type == LiteralType.BOOL:
            dopevar = DopeBool.create_variable(name)
        elif type == LiteralType.STRING:
            dopevar = DopeString.create_variable(name)
        elif type == LiteralType.INTSET:
            dopevar = DopeIntSet.create_variable(name)
        elif type == LiteralType.STRINGSET:
            dopevar = DopeStringSet.create_variable(name)
        else:
            raise ValueError("Unsupported attribute type {}".format(type))
        super().__init__(dopevar)
