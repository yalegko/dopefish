from enum import Enum

from dopefish.dopetypes import DopeBool
from .interfaces import IEvaluable


class Operator(Enum):
    # Comparision operators
    EQ = "=="
    NEQ = "!="
    LT = "<"
    GT = ">"
    LE = "<="
    GE = ">="
    # Logical operators
    AND = "AND"
    OR = "OR"
    NOT = "NOT"
    # Array operators
    IN = "IN"
    SUBSET = "SUBSET"
    SUBSETEQ = "SUBSETEQ"
    INTERSECTS = "INTERSECTS"


class Expression(IEvaluable):
    def __init__(self,
                 left: IEvaluable,
                 operator: Operator,
                 right: IEvaluable):
        # Here is some kind of lazy-evaluation
        self.left = left
        self.operator = operator
        self.right = right

    def evaluate(self) -> DopeBool:
        # According to the parsing order
        if self.operator == Operator.NOT:
            return ~ self.left.evaluate()
        elif self.operator == Operator.EQ:
            return self.left.evaluate() == self.right.evaluate()
        elif self.operator == Operator.NEQ:
            return self.left.evaluate() != self.right.evaluate()
        elif self.operator == Operator.LT:
            return self.left.evaluate() < self.right.evaluate()
        elif self.operator == Operator.LE:
            return self.left.evaluate() <= self.right.evaluate()
        elif self.operator == Operator.GT:
            return self.left.evaluate() > self.right.evaluate()
        elif self.operator == Operator.GE:
            return self.left.evaluate() >= self.right.evaluate()
        elif self.operator == Operator.OR:
            return self.left.evaluate() | self.right.evaluate()
        elif self.operator == Operator.AND:
            return self.left.evaluate() & self.right.evaluate()
        elif self.operator == Operator.IN:
            return self.right.evaluate().contains(self.left.evaluate())
        elif self.operator == Operator.SUBSET:
            return self.left.evaluate().issubset(self.right.evaluate())
        elif self.operator == Operator.SUBSETEQ:
            return self.left.evaluate().issubseteq(self.right.evaluate())
        elif self.operator == Operator.INTERSECTS:
            return self.left.evaluate().intersects(self.right.evaluate())
        else:
            raise ValueError("Unsupported operator {}", self.operator)
