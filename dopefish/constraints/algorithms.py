from abc import ABC, abstractmethod
from functools import reduce

import z3

from .interfaces import ICombinable


class CombiningResult(ICombinable):
    def __init__(self, permit, deny, indeterminate, not_applicable):
        self._permit = permit
        self._deny = deny
        self._indeterminate = indeterminate
        self._not_applicable = not_applicable

    @property
    def permit(self):
        return self._permit

    @property
    def deny(self):
        return self._deny

    @property
    def not_applicable(self):
        return self._not_applicable

    @property
    def indeterminate(self):
        return self._indeterminate

    def __repr__(self): # pragma: no cover
        return "<p={} d={} n={} i={}>".format(
            z3.simplify(self.permit), z3.simplify(self.deny),
            z3.simplify(self.not_applicable), z3.simplify(self.indeterminate),
        )


class AbstractAlgorithm(ABC):
    @classmethod
    def combine(cls, *results: ICombinable)-> CombiningResult:
        if len(results) < 1:
            raise ValueError("Nothing to combine")
        if len(results) == 1:
            return cls._trait_single(results[0])
        return reduce(cls._combine, results)

    @classmethod
    def _trait_single(cls, res: ICombinable) -> CombiningResult:
        return CombiningResult(
            permit=res.permit,
            deny=res.deny,
            indeterminate=res.indeterminate,
            not_applicable=res.not_applicable
        )

    @classmethod
    @abstractmethod
    def _combine(cls, this: ICombinable, other: ICombinable) -> CombiningResult:
        pass # pragma: no cover


class PermitOverrides(AbstractAlgorithm):
    @classmethod
    def _combine(cls, this: ICombinable, other: ICombinable) -> CombiningResult:
        return CombiningResult(
            permit=z3.Or(this.permit, other.permit),
            deny=z3.Or(
                z3.And(this.deny, other.deny),
                z3.And(this.deny, other.not_applicable),
                z3.And(this.not_applicable, other.deny)
            ),
            not_applicable=z3.And(this.not_applicable, other.not_applicable),
            indeterminate=z3.Or(
                z3.And(this.indeterminate, z3.Not(other.permit)),
                z3.And(z3.Not(this.permit), other.indeterminate)
            )
        )


class DenyOverrides(AbstractAlgorithm):
    @classmethod
    def _combine(cls, this: ICombinable, other: ICombinable) -> CombiningResult:
        return CombiningResult(
            permit=z3.Or(
                z3.And(this.permit, other.permit),
                z3.And(this.permit, other.not_applicable),
                z3.And(this.not_applicable, other.permit)
            ),
            deny=z3.Or(this.deny, other.deny),
            not_applicable=z3.And(this.not_applicable, other.not_applicable),
            indeterminate=z3.Or(
                z3.And(this.indeterminate, z3.Not(other.deny)),
                z3.And(z3.Not(this.deny), other.indeterminate)
            )
        )


class DenyUnlessPermit(AbstractAlgorithm):
    @classmethod
    def _trait_single(cls, res: ICombinable):
        return CombiningResult(
            permit=res.permit,
            deny=z3.Or(
                res.deny, res.not_applicable, res.indeterminate
            ),
            not_applicable=z3.BoolVal(False),
            indeterminate=z3.BoolVal(False)
        )

    @classmethod
    def _combine(cls, this: ICombinable, other: ICombinable) -> CombiningResult:
        return CombiningResult(
            permit=z3.Or(this.permit, other.permit),
            deny=z3.And(
                z3.Not(this.permit),
                z3.Not(other.permit),
                z3.Or(this.deny, this.not_applicable, this.indeterminate),
                z3.Or(other.deny, other.not_applicable, other.indeterminate),
            ),
            not_applicable=z3.BoolVal(False),
            indeterminate=z3.BoolVal(False)
        )


class PermitUnlessDeny(AbstractAlgorithm):
    @classmethod
    def _trait_single(cls, res: ICombinable):
        return CombiningResult(
            permit=z3.Or(
                res.permit, res.not_applicable, res.indeterminate
            ),
            deny=res.deny,
            not_applicable=z3.BoolVal(False),
            indeterminate=z3.BoolVal(False)
        )

    @classmethod
    def _combine(cls, this: ICombinable, other: ICombinable) -> CombiningResult:
        return CombiningResult(
            permit=z3.And(
                z3.Not(this.deny),
                z3.Not(other.deny),
                z3.Or(this.permit, this.not_applicable, this.indeterminate),
                z3.Or(other.permit, other.not_applicable, other.indeterminate),
            ),
            deny=z3.Or(this.deny, other.deny),
            not_applicable=z3.BoolVal(False),
            indeterminate=z3.BoolVal(False)
        )


class FirstApplicable(AbstractAlgorithm):
    @classmethod
    def _combine(cls, this: ICombinable, other: ICombinable) -> CombiningResult:
        return CombiningResult(
            permit=z3.Or(
                this.permit,
                z3.And(this.not_applicable, other.permit)
            ),
            deny=z3.Or(
                this.deny,
                z3.And(this.not_applicable, other.deny)
            ),
            not_applicable=z3.And(this.not_applicable, other.not_applicable),
            indeterminate=z3.Or(
                this.indeterminate,
                z3.And(this.not_applicable, other.indeterminate)
            ),
        )


class OnlyOneApplicable(AbstractAlgorithm):
    @classmethod
    def _combine(cls, this: ICombinable, other: ICombinable) -> CombiningResult:
        return CombiningResult(
            permit=z3.Or(
                z3.And(this.permit, other.not_applicable),
                z3.And(this.not_applicable, other.permit)
            ),
            deny=z3.Or(
                z3.And(this.deny, other.not_applicable),
                z3.And(this.not_applicable, other.deny)
            ),
            not_applicable=z3.And(this.not_applicable, other.not_applicable),
            indeterminate=z3.Or(
                this.indeterminate, other.indeterminate,
                z3.And(
                    z3.Or(this.permit, this.deny),
                    z3.Or(other.permit, other.deny),
                )
            ),
        )
