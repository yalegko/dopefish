from abc import abstractmethod
from typing import List, Type

import z3

from .algorithms import AbstractAlgorithm
from .interfaces import ICombinable, combine_targets, IEvaluable
from .rule import Rule


class AbstractResultCombinator(ICombinable):
    @property
    @abstractmethod
    def combining_result(self):
        pass # pragma: no cover

    @property
    @abstractmethod
    def target_result(self):
        pass # pragma: no cover

    @property
    def permit(self):
        return z3.And(
            self.target_result.get_constraint(True),
            self.combining_result.permit
        )

    @property
    def deny(self):
        return z3.And(
            self.target_result.get_constraint(True),
            self.combining_result.deny
        )

    @property
    def not_applicable(self):
        return z3.Or(
            self.target_result.get_constraint(False),
            z3.And(
                self.target_result.get_constraint(True),
                self.combining_result.not_applicable
            )
        )

    @property
    def indeterminate(self):
        return z3.Or(
            self.target_result.is_missing(),
            self.target_result.is_error(),
            z3.And(
                self.target_result.get_constraint(True),
                self.combining_result.indeterminate
            )
        )


class Policy(AbstractResultCombinator):
    def __init__(self,
                 name: str,
                 algorithm: Type[AbstractAlgorithm],
                 targets: List[IEvaluable],
                 rules: List[Rule]):
        self.name = name
        self.algorithm = algorithm
        self.targets = targets
        self.rules = rules

        self._target_result = combine_targets(*targets)
        self._combining_result = algorithm.combine(*rules)

    @property
    def target_result(self):
        return self._target_result

    @property
    def combining_result(self):
        return self._combining_result


class PolicySet(AbstractResultCombinator):
    def __init__(self,
                 name: str,
                 algorithm: Type[AbstractAlgorithm],
                 targets: List[IEvaluable],
                 policies: List[Policy],
                 policy_sets: List["PolicySet"]):
        self.name = name
        self.algorithm = algorithm
        self.targets = targets
        self.policies = policies
        self.policy_sets = policy_sets

        self._target_result = combine_targets(*targets)
        self._combining_result = algorithm.combine(*policies, *policy_sets)

    @property
    def target_result(self):
        return self._target_result

    @property
    def combining_result(self):
        return self._combining_result
