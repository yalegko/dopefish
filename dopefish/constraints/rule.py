from enum import Enum

import z3

from .interfaces import ICombinable, combine_targets


class Effect(Enum):
    PERMIT = "permit"
    DENY = "deny"


class Rule(ICombinable):
    def __init__(self, name, effect, targets):
        self.name = name
        self.effect = effect
        self.targets = targets

        self._target_expression = combine_targets(*self.targets)

    @property
    def permit(self):
        if self.effect == Effect.PERMIT:
            return self._target_expression.get_constraint(True)
        elif self.effect == Effect.DENY:
            return z3.BoolVal(False)
        else:
            raise ValueError("Unsupported effect {}".format(self.effect))

    @property
    def deny(self):
        if self.effect == Effect.PERMIT:
            return z3.BoolVal(False)
        elif self.effect == Effect.DENY:
            return self._target_expression.get_constraint(True)
        else:
            raise ValueError("Unsupported effect {}".format(self.effect))

    @property
    def not_applicable(self):
        return self._target_expression.get_constraint(False)

    @property
    def indeterminate(self):
        """ Probably here we should check for type (or should not) """
        return z3.Or(
            self._target_expression.is_missing(),
            self._target_expression.is_error()
        )
