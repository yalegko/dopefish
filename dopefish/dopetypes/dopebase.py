from abc import ABC, abstractmethod

import z3


def _is_flags():
    return (("is_missing", z3.BoolSort()), ("is_err", z3.BoolSort()))


class DopeBase(ABC):

    def __init__(self, val=None, is_missing=False, is_err=False):
        if val is None:
            val = self.__class__.default_value()
        self.z3obj = self.__class__.build(val, is_missing, is_err)
        self.inner_z3var = val

    @classmethod
    def from_expression(cls, z3expr):
        obj = cls()
        obj.z3obj = z3expr
        return obj

    @classmethod
    @abstractmethod
    def build(cls, val, is_missing=False, is_err=False):
        pass # pragma: no cover

    @classmethod
    @abstractmethod
    def default_value(cls):
        pass # pragma: no cover

    @classmethod
    def create_value(cls, val):
        return cls.build(val, False, False)

    @classmethod
    def create_variable(cls, variable_name):
        return cls(cls.create_z3_variable(variable_name))

    @classmethod
    @abstractmethod
    def create_z3_variable(cls, name):
        pass # pragma: no cover

    @classmethod
    def create_missing(cls):
        return cls.build(cls.default_value(), True, False)

    @classmethod
    def create_error(cls):
        return cls.build(cls.default_value(), False, True)

    @property
    @abstractmethod
    def datatype(self):
        pass # pragma: no cover

    def get_val(self):
        return self.datatype.val(self.z3obj)

    def is_missing(self):
        return self.datatype.is_missing(self.z3obj)

    def is_error(self):
        return self.datatype.is_err(self.z3obj)

    def has_value(self):
        """
        We suppose that the only field of tuple if filled. So it's the value when
        no error or missing attribute occurred.
        """
        return z3.And(
            z3.Not(self.is_missing()),
            z3.Not(self.is_error())
        )
