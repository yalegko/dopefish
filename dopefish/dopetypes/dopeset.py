import string
import time
from abc import abstractmethod
from random import choice
from typing import Iterable, Any

import z3

from . import helpers
from .dopebase import DopeBase, _is_flags
from .dopebool import DopeBool


class DopeBaseSet(DopeBase):
    def __init__(self, val=None, is_missing=False, is_err=False,
                 additional_constraints=None):
        super().__init__(val, is_missing, is_err)
        self.additional_constraints = additional_constraints or []

    @classmethod
    @abstractmethod
    def element_var(cls, name):
        pass # pragma: no cover

    @classmethod
    @abstractmethod
    def element_value(cls, name):
        pass # pragma: no cover

    @classmethod
    @abstractmethod
    def element_sort(cls):
        pass # pragma: no cover

    @classmethod
    def from_values(cls, values: Iterable[Any]):
        name = "array_literal_{}{}".format(
            time.time(), choice(string.ascii_uppercase)
        )
        arr = z3.Array(name, z3.IntSort(), cls.element_sort())
        values = [cls.element_value(v) for v in  set(values)]
        for i, v in enumerate(values):
            arr = z3.Update(arr, i, v)
        x = cls.element_var("x")
        return cls(arr,
                   additional_constraints=[
                       z3.Not(z3.Exists([x],
                           z3.And(
                              DopeBaseSet._contains(arr, x),
                              z3.Distinct(x, *values)
                           ))
                       )
                ])

    @classmethod
    def create_z3_variable(cls, variable_name):
        return z3.Array(variable_name, z3.IntSort(), cls.element_sort())

    @classmethod
    def default_value(cls):  # pragma: no cover
        raise NotImplemented("Should not be used")

    def contains(self, item) -> DopeBool:
        return helpers.cmp_expression(self, DopeBaseSet._contains, item)

    def issubseteq(self, set) -> DopeBool:
        return helpers.cmp_expression(self, self._issubseteq, set)

    def issubset(self, set) -> DopeBool:
        def expression(subset, set):
            i, j = z3.Ints("i j")
            return z3.And(
                self._issubseteq(subset, set),
                z3.Exists([i],
                          z3.Not(z3.Exists([j], set[i] == subset[j])))
            )

        return helpers.cmp_expression(self, expression, set)

    def intersects(self, other) -> DopeBool:
        def expression(this, other):
            i, j = z3.Ints("i j")
            return z3.Exists([i,j], this[i] == other[j])
        return helpers.cmp_expression(self, expression, other)

    @staticmethod
    def  _contains(set, element):
        i = z3.Int("i")
        return z3.Exists([i], set[i] == element)

    def _issubseteq(self, subset, set):
        x = self.element_var("x")
        return z3.ForAll([x],
                         z3.Implies(
                             DopeBaseSet._contains(subset, x),
                             DopeBaseSet._contains(set, x),
                         ))


class DopeStringSet(DopeBaseSet):
    datatype = z3.Datatype("DopeStringSet")
    datatype.declare("stringSet",
                     ("val", z3.ArraySort(z3.IntSort(), z3.StringSort())),
                     *_is_flags())
    datatype = datatype.create()

    @classmethod
    def build(cls, val, is_missing=False, is_err=False):
        return cls.datatype.stringSet(val, is_missing, is_err)

    @classmethod
    def element_sort(cls):
        return z3.StringSort()

    @classmethod
    def element_var(cls, name):
        return z3.String(name)

    @classmethod
    def element_value(cls, v):
        return z3.StringVal(v)


class DopeIntSet(DopeBaseSet):
    datatype = z3.Datatype("DopeIntSet")
    datatype.declare("intSet",
                     ("val", z3.ArraySort(z3.IntSort(), z3.IntSort())),
                     *_is_flags())
    datatype = datatype.create()

    @classmethod
    def build(cls, val, is_missing=False, is_err=False):
        return cls.datatype.intSet(val, is_missing, is_err)

    @classmethod
    def element_sort(cls):
        return z3.IntSort()

    @classmethod
    def element_var(cls, name):
        return z3.Int(name)

    @classmethod
    def element_value(cls, v):
        return z3.IntVal(v)
