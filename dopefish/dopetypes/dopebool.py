import z3

from .dopebase import DopeBase, _is_flags


class DopeBool(DopeBase):
    datatype = z3.Datatype("DopeBool")
    datatype.declare("bool", ("val", z3.BoolSort()), *_is_flags())
    datatype = datatype.create()

    @classmethod
    def build(cls, val, is_missing=False, is_err=False):
        return cls.datatype.bool(val, is_missing, is_err)

    @classmethod
    def default_value(cls):
        return False

    @classmethod
    def create_z3_variable(cls, name):
        return z3.Bool(name)

    def get_constraint(self, result=True):
        return self.z3obj == DopeBool.create_value(result)

    def is_true(self):
        return z3.If(self.z3obj == DopeBool.create_value(True), True, False)

    def is_false(self):
        return z3.If(self.z3obj == DopeBool.create_value(False), True, False)

    def __invert__(self):
        """ That like boolean NOT for DopeBool but using overloaded ~ """
        return DopeBool.from_expression(
            z3.If(self.is_true(),
                  DopeBool.create_value(False),
                  z3.If(self.is_false(),
                        DopeBool.create_value(True),
                        z3.If(self.is_missing(),
                              DopeBool.create_missing(),
                              DopeBool.create_error()
                              )
                        )
                  )
        )

    def __and__(self, other):
        """ That like boolean AND for DopeBool but using overloaded & """
        return DopeBool.from_expression(
            z3.If(z3.And(self.is_true(), other.is_true()),
                  DopeBool.create_value(True),
                  z3.If(z3.Or(self.is_false(), other.is_false()),
                        DopeBool.create_value(False),
                        z3.If(z3.Or(self.is_error(), other.is_error()),
                              DopeBool.create_error(),
                              DopeBool.create_missing(),
                              )
                        )
                  )
        )

    def __or__(self, other):
        """ That like boolean OR for DopeBool but using overloaded | """
        return DopeBool.from_expression(
            z3.If(z3.Or(self.is_true(), other.is_true()),
                  DopeBool.create_value(True),
                  z3.If(z3.Or(self.is_error(), other.is_error()),
                        DopeBool.create_error(),
                        z3.If(z3.Or(self.is_missing(), other.is_missing()),
                              DopeBool.create_missing(),
                              DopeBool.create_value(False)
                              )
                        )
                  )
        )

    def __eq__(self, other):
        """ Probably here we should check for type (or should not) """
        return DopeBool.from_expression(
            z3.If(z3.Or(self.is_error(), other.is_error()),
                  DopeBool.create_error(),
                  z3.If(z3.Or(self.is_missing(), other.is_missing()),
                        DopeBool.create_missing(),
                        z3.If(self.get_val() == other.get_val(),
                              DopeBool.create_value(True),
                              DopeBool.create_value(False),
                              )
                        )
                  )
        )

    def __ne__(self, other):
        """ Probably here we should check for type (or should not) """
        return DopeBool.from_expression(
            z3.If(z3.Or(self.is_error(), other.is_error()),
                  DopeBool.create_error(),
                  z3.If(z3.Or(self.is_missing(), other.is_missing()),
                        DopeBool.create_missing(),
                        z3.If(self.get_val() != other.get_val(),
                              DopeBool.create_value(True),
                              DopeBool.create_value(False),
                              )
                        )
                  )
        )
