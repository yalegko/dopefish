import z3

from .dopebool import DopeBool


def cmp_expression(self, expression, other) -> DopeBool:
    """ Probably here we should check for type (or should not) """
    return DopeBool.from_expression(
        z3.If(z3.Or(self.is_error(), other.is_error()),
              DopeBool.create_error(),
              z3.If(z3.Or(self.is_missing(), other.is_missing()),
                    DopeBool.create_missing(),
                    z3.If(expression(self.get_val(), other.get_val()),
                          DopeBool.create_value(True),
                          DopeBool.create_value(False),
                          )
                    )
              )
    )


def arith_expr(self, expression, other, base_type):
    """ Probably here we should check for type (or should not) """
    return base_type.from_expression(
        z3.If(z3.And(self.has_value(), other.has_value()),
              base_type.create_value(expression(self.get_val(), other.get_val())),
              z3.If(z3.Or(self.is_error(), other.is_error()),
                    base_type.create_error(),
                    base_type.create_missing()
                    )
              )
    )
