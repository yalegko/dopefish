import z3

from . import helpers
from .dopebase import DopeBase, _is_flags


class DopeString(DopeBase):
    datatype = z3.Datatype("DopeString")
    datatype.declare("string", ("val", z3.StringSort()), *_is_flags())
    datatype = datatype.create()

    @classmethod
    def build(cls, val, is_missing=False, is_err=False):
        if isinstance(val, str):
            return cls.datatype.string(z3.StringVal(val), is_missing, is_err)
        elif z3.is_string_value(val) or z3.is_string(val):
            return cls.datatype.string(val, is_missing, is_err)
        else:
            raise ValueError("Unsupported argument {}; expected str or z3.String var/value".format(val))

    @classmethod
    def default_value(cls):
        return ""

    @classmethod
    def create_z3_variable(cls, name):
        return z3.String(name)

    def __eq__(self, other):
        """ Probably here we should check for type (or should not) """
        return helpers.cmp_expression(self, lambda s, o: s == o, other)

    def __ne__(self, other):
        """ Probably here we should check for type (or should not) """
        return helpers.cmp_expression(self, lambda s, o: s != o, other)