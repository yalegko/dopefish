import z3

from . import helpers
from .dopebase import DopeBase, _is_flags


class DopeInt(DopeBase):
    datatype = z3.Datatype("DopeInt")
    datatype.declare("int", ("val", z3.IntSort()), *_is_flags())
    datatype = datatype.create()

    @classmethod
    def build(cls, val, is_missing=False, is_err=False):
        return cls.datatype.int(val, is_missing, is_err)

    @classmethod
    def default_value(cls):
        return 0

    @classmethod
    def create_z3_variable(cls, name):
        return z3.Int(name)

    def __arith_expr__(self, expr, other):
        """ Probably here we should check for type (or should not) """
        return helpers.arith_expr(self, expr, other, base_type=DopeInt)

    def __add__(self, other):
        return self.__arith_expr__(lambda s,o: s + o, other)

    def __mul__(self, other):
        return self.__arith_expr__(lambda s,o: s * o, other)

    def __sub__(self, other):
        return self.__arith_expr__(lambda s,o: s - o, other)

    def __truediv__(self, other):
        return self.__arith_expr__(lambda s, o: s / o, other)

    def __lt__(self, other):
        return helpers.cmp_expression(self, lambda s, o: s < o, other)

    def __le__(self, other):
        return helpers.cmp_expression(self, lambda s, o: s <= o, other)

    def __eq__(self, other):
        return helpers.cmp_expression(self, lambda s, o: s == o, other)

    def __ne__(self, other):
        return helpers.cmp_expression(self, lambda s, o: s != o, other)

    def __gt__(self, other):
        return helpers.cmp_expression(self, lambda s, o: s > o, other)

    def __ge__(self, other):
        return helpers.cmp_expression(self, lambda s, o: s >= o, other)
