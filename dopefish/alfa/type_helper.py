from typing import List, Optional

from aule.codegen import ust

from dopefish import constraints


class TypeHelper:
    # Annotation that marks aNgine entities
    _annotaiton_prefix = "angine="
    # List of possible entities we can face in
    _supported_entities = ["entity", "subject"]
    # List of types supported by checking system
    _supported_types = {
        ust.SimpleType.BOOLEAN: constraints.LiteralType.BOOL,
        ust.SimpleType.INTEGER: constraints.LiteralType.INT,
        ust.SimpleType.STRING: constraints.LiteralType.STRING,
    }
    _array_types = {
        ust.SimpleType.INTEGER: constraints.LiteralType.INTSET,
        ust.SimpleType.STRING: constraints.LiteralType.STRINGSET,
    }

    def __init__(self, classes: List[ust.ClassDeclaration]):
        self.entities = {}
        for cls in classes:
            for ann in cls.annotations:
                if ann.value.startswith(TypeHelper._annotaiton_prefix):
                    entity_type = ann.value[len(TypeHelper._annotaiton_prefix):]
                    if entity_type in TypeHelper._supported_entities:
                        self.entities[entity_type] = cls
        if len(self.entities) != len(TypeHelper._supported_entities):
            raise ValueError(
                "Wrong number of aNgine entities found; expected {} found {}".format(
                    TypeHelper._supported_entities, self.entities.keys()
                )
            )
        self.reference = ust.helpers.ClassesReference(
            classes=classes
        )

    def resolve_type(self,attribute_access: str) -> constraints.LiteralType:
        """ Uses ClassesReference to resolve the type of attribute access
        :param attribute_access: attribute access string in a form:
               `receiver.attribute` where `receiver` is one of @_supported_entities
               and attribute is a field name of the receiver.
        """
        try:
            entity_type, attr = attribute_access.split('.')
        except ValueError:
            raise ValueError(
                "Unsupported entity access type; got '{}' expected to have only one dot".format(
                    attribute_access
                )
            )
        entity = self.entities.get(entity_type)
        if entity is None:
            raise ValueError(
                "Unknown entity type {} in attribute access expression `{}`".format(
                    entity_type, attribute_access
                )
            )
        field = self.reference.get_field(entity, attr)
        if field is None:
            raise ValueError(
                "Unknown attribute `{}` for entity `{}` represented by {}".format(
                    attr, entity_type, entity
                )
            )
        return TypeHelper._extract_type(field.type)

    @staticmethod
    def _extract_type(type_node: ust.TypeReference) -> constraints.LiteralType:
        """ Extracts SimpleType from the TypeReference node or raise ValueError
            if the type is unsupported.

            Supported types are SimpleType's and their sequences and nullable
            unions (will be missing attribute in such case).
        """
        simple_type = None
        if isinstance(type_node.type, ust.SimpleType):
            simple_type = type_node.type
        elif isinstance(type_node.type, ust.UnionType):
            simple_type = TypeHelper._extract_simple_nullable(type_node.type)
        if simple_type is None or simple_type not in TypeHelper._supported_types:
            raise TypeError("Unsupported type {}".format(type_node))
        if type_node.is_sequence and simple_type not in TypeHelper._array_types:
            raise TypeError("Unsupported set type {}".format(type_node))
        if type_node.is_sequence:
            return TypeHelper._array_types[simple_type]
        else:
            return TypeHelper._supported_types[simple_type]

    @staticmethod
    def _extract_simple_nullable(node: ust.UnionType) -> Optional[ust.SimpleType]:
        """ Returns SimpleType iff node is a union of a SimpleType and NULL
            o/w returns None.
        """
        if len(node.types) != 2:
            return None
        if ust.SimpleType.NULL not in (x.type for x in node.types):
            return None
        if node.types[0].type == ust.SimpleType.NULL:
            second_type = node.types[1].type
        else:
            second_type = node.types[0].type
        if not isinstance(second_type, ust.SimpleType):
            return None
        return  second_type