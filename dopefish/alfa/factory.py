from typing import Union, List, Dict

import aule

from dopefish.constraints import PolicySet, Policy, AbstractLiteral
from .type_helper import TypeHelper
from .visitor import Visitor


class ConstraintsTree:
    def __init__(self,
                 policies: List[Union[PolicySet, Policy]],
                 attributes: Dict[str, AbstractLiteral],
                 additional_constraints):
        self.policies = policies
        self.attributes = {
            name: literal.evaluate() for name, literal in attributes.items()
        }
        self.additional_constraints = additional_constraints


class ConstraintsFactory:
    @staticmethod
    def build_tree(alfa_policy: str, idl_interfaces: str) -> ConstraintsTree:
        """ Translates policy in ALFA and entities description in IDL into
            the tree of `dopefish.constraints`
        """
        alfa_policy = aule.ASTParserFactory\
            .create("alfa")\
            .parse(alfa_policy)
        if len(alfa_policy.body) < 1:
            raise SyntaxError("Empty ALFA script body")
        namespace = alfa_policy.body[0]
        if len(namespace.body) < 1:
            raise SyntaxError("Empty policy namespace body")
        interfaces = aule.ASTParserFactory\
            .create("idl")\
            .parse(idl_interfaces)
        ast_visitor = Visitor(
            type_helper=TypeHelper(interfaces)
        )
        return ConstraintsTree(
            policies=[
                ast_visitor.visit(p) for p in namespace.body
            ],
            attributes=ast_visitor.attributes,
            additional_constraints=ast_visitor.additional_constraints,
        )
