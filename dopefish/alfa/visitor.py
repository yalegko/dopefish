from aule.generated import alfaAST

from dopefish import constraints
from dopefish.constraints import LiteralArray
from .type_helper import TypeHelper


class Visitor:
    def __init__(self, type_helper: TypeHelper):
        self.type_helper = type_helper
        self.attributes = {}
        self.additional_constraints = []

    def visit(self, target):
        try:
            return getattr(self, "visit{}".format(target.__class__.__name__))(target)
        except:
            raise ValueError("Unsupported AST node {}".format(target))

    def visitPolicySetDeclaration(self, target: alfaAST.PolicySetDeclaration):
        return constraints.PolicySet(
            name=target.name,
            algorithm=self.visit(target.algorithm),
            targets=self._createTargets(target.target),
            policies=[
                self.visit(p) for p in target.policies
            ],
            policy_sets=[
                self.visit(p) for p in target.policysets
            ]
        )

    def visitPolicyDeclaration(self, target: alfaAST.PolicyDeclaration):
        return constraints.Policy(
            name=target.name,
            algorithm=self.visit(target.algorithm),
            targets=self._createTargets(target.target),
            rules=[
                self.visit(r) for r in target.rules
            ]
        )

    def visitRuleDeclaration(self, target: alfaAST.RuleDeclaration):
        return constraints.Rule(
            name=target.name,
            effect=self.visit(target.effect),
            targets=self._createTargets(target.target),
        )

    def _createTargets(self, target: alfaAST.TargetStatement):
        if target is None:
            return []
        return [
            self.visit(t) for t in target.clauses
        ]

    def visitApplyStatement(self, target: alfaAST.ApplyStatement):
        algorithm_name = target.value.lower()
        if algorithm_name == "denyunlesspermit":
            return constraints.DenyUnlessPermit
        elif algorithm_name == "permitunlessdeny":
            return constraints.PermitUnlessDeny
        elif algorithm_name == "permitoverrides":
            return constraints.PermitOverrides
        elif algorithm_name == "denyoverrides":
            return constraints.DenyOverrides
        elif algorithm_name == "firstapplicable":
            return constraints.FirstApplicable
        elif algorithm_name == "onlyoneapplicable":
            return constraints.OnlyOneApplicable
        else:
            raise ValueError(
                "Unknown combining algorithm type {}".format(target.value)
            )

    def visitEffectStatement(self, target: alfaAST.EffectStatement):
        effect_name = target.value.lower()
        if effect_name == "permit":
            return constraints.Effect.PERMIT
        elif effect_name == "deny":
            return constraints.Effect.DENY
        else:
            raise ValueError("Unsupported effect {}".format(target.value))

    @staticmethod
    def visitOperator(operator):
        try:
            return constraints.Operator(operator.upper())
        except:
            raise ValueError("Unsupported operator {}".format(operator))

    def visitBinaryExpression(self, target: alfaAST.BinaryExpression):
        return constraints.Expression(
            left=self.visit(target.left),
            operator=Visitor.visitOperator(target.operator),
            right=self.visit(target.right)
        )

    def visitLogicalExpression(self, target: alfaAST.LogicalExpression):
        return constraints.Expression(
            left=self.visit(target.left),
            operator=Visitor.visitOperator(target.operator),
            right=self.visit(target.right)
        )

    def visitUnaryExpression(self, target: alfaAST.UnaryExpression):
        return constraints.Expression(
            left=self.visit(target.left),
            operator=Visitor.visitOperator(target.operator),
            right=None
        )

    def visitAnyExpression(self, target: alfaAST.AnyExpression):
        return constraints.Expression(
            left=self.visit(target.left),
            operator=constraints.Operator.INTERSECTS,
            right=self.visit(target.right)
        )

    def visitArrayExpression(self, target: alfaAST.ArrayExpression):
        res = LiteralArray([
            e.value for e in target.elements
        ])
        self.additional_constraints.extend(res.additional_constraints())
        return res

    def visitLiteralString(self, target: alfaAST.LiteralString):
        return constraints.LiteralString(target.value.strip('"'))

    def visitLiteralNumeric(self, target: alfaAST.LiteralNumeric):
        return constraints.LiteralInt(int(target.value))

    def visitLiteralBoolean(self, target: alfaAST.LiteralBoolean):
        value = target.value.lower()
        if value not in ["true", "false"]:
            raise ValueError("Unknown boolean literal {}".format(target.value))
        return constraints.LiteralBool(value == "true")

    @staticmethod
    def getAttributeName(target: alfaAST.AttributeAccessExpression):
        if isinstance(target, alfaAST.Identifier):
            return target.name
        return Visitor.getAttributeName(target.expression) + "." + target.name

    def visitAttributeAccessExpression(self, target: alfaAST.AttributeAccessExpression):
        access_string = Visitor.getAttributeName(target)
        if access_string not in self.attributes:
            self.attributes[access_string] = constraints.LiteralAttribute(
                name=access_string,
                type=self.type_helper.resolve_type(access_string)
            )
        return self.attributes[access_string]

    def visitIdentifier(self, target: alfaAST.Identifier):
        # Actually the only possible node here is `action` variable
        action = target.name
        if action not in self.attributes:
            self.attributes[action] = constraints.LiteralAttribute(
                name=action,
                type=constraints.LiteralType.STRING
            )
        return self.attributes[action]